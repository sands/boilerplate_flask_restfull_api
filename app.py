#############################################
########## CLEAN ARCHITECTURE BY SANDS.io
#############################################
from dotenv import dotenv_values, load_dotenv
from flask import Flask, Blueprint, jsonify
from flask_jwt_extended import JWTManager
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cli import FlaskCLI
import os

load_dotenv()


class Config:
    MYSQL_USERNAME = os.getenv("MYSQL_USERNAME")
    MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD")
    MYSQL_HOST = os.getenv("MYSQL_HOST")
    MYSQL_PORT = os.getenv("MYSQL_PORT")
    MYSQL_DB = os.getenv("MYSQL_DB")


SQLALCHEMY_DATABASE_URL = (
    f"mysql+mysqlconnector://{Config.MYSQL_USERNAME}:{Config.MYSQL_PASSWORD}@"
    f"{Config.MYSQL_HOST}:{Config.MYSQL_PORT}/{Config.MYSQL_DB}"
)
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URL
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["JWT_SECRET_KEY"] = "YOURSECRETKEY"
app.config["SECRET_KEY"] = "YOURSECRETKEY"

#############################################
########## JWT
#############################################
jwt = JWTManager(app)


# Unauthorized handler
@jwt.unauthorized_loader
def unauthorized_callback(callback):
    return jsonify(message="Unauthorized"), 401


# Invalid token handler
@jwt.invalid_token_loader
def invalid_token_callback(callback):
    return jsonify(message="Invalid token"), 401


# Expired token handler
@jwt.expired_token_loader
def expired_token_callback(callback):
    return jsonify(message="Token has expired"), 401


#############################################
########## Database
#############################################
from db.sql import initialize_database_app

db = initialize_database_app(app)
# Migration
migrate = Migrate(app, db)
from models import *

# Seeding Data
from db.seeder import *

FlaskCLI(app)


@app.cli.command()
def seedingdata():
    print("Executing seeding_data command!")
    run_seeding()
    # Clear SQLAlchemy's metadata cache
    db.metadata.clear()


#############################################
########## Blueprints
#############################################
from api.v1 import api_v1

app.register_blueprint(api_v1, url_prefix="/api/v1")


#############################################
########## Run
#############################################
if __name__ == "__main__":
    app.run(debug=True)
    # app.run()
