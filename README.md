############  FLASK BOILERPLATE REST API by SANDS #####################

# Flask REST API with Flask, SQLAlchemy, JWT Auth, and MySQL

This project is a Flask REST API that uses Flask for API development, SQLAlchemy for database management,
JWT Auth for authentication, and MySQL for storage. The API also includes Swagger UI for documentation and testing.

## Installation

To install the necessary dependencies for this project, run the following command:

```
pip install -r requirements.txt
```

This will install all the necessary libraries and modules, including Flask, SQLAlchemy, and JWT Auth.

## Running the Project

To run the project, follow these steps:

1. Modify the `.env` file with the necessary environment variables.
2. Run the following command:

```
flask run
```

This will start the Flask development server and allow you to access the API from your local machine.


## MIGRATION DATABASE
1. Initialize the migrations folder:
flask db init
2. Generate an initial migration script:
flask db migrate -m "Initial migration"
3. Apply the migration to the database:
flask db upgrade

## SEEDING DATA 
flask seedingdata


## Set the FLASK_APP environment variable to the file that contains your Flask application. Here's how you can do it:
-For Windows (Command Prompt):
set FLASK_APP=app.py
-For Windows (PowerShell):
$env:FLASK_APP = "app.py"
-For Unix/Linux (Bash):
export FLASK_APP=app.py

## Deploying Flask Application on Ubuntu (Apache+WSGI)
sudo apt-get update 
sudo apt-get upgrade 
sudo apt-get install python3 python3-pip python3-venv 
sudo apt-get install apache2 libapache2-mod-wsgi-py3

python3 -m venv venv 
source venv/bin/activate 

export FLASK_APP=app.py 
flask run

nano /var/www/flaskapp/flaskapp.wsgi 

#Configuring Apache
<VirtualHost *:9905>
  WSGIDaemonProcess app_name user=www-data group=www-data threads=5
  WSGIScriptAlias / /var/www/html/app_name/app.wsgi
  #WSGIScriptAlias / /var/www/html/flaskdemo/app.wsgi
  WSGIScriptReloading On

   <Directory /var/www/html/app_name>
        Allow from all
        Require all granted
    </Directory>

  ErrorLog /var/www/html/app_name/error.log
  CustomLog /var/www/html/app_name/access.log combined
</VirtualHost>
