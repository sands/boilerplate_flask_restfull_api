from flask_sqlalchemy import SQLAlchemy

# Prepare the database
init_db = None

def initialize_database_app(app):
    init_db = SQLAlchemy(app)
    return init_db 
    # set_db = db.init_app(app)
    # return set_db

#############################################
########## Utility Classes/Constants
#############################################
# Class to add, update and delete data via SQLALchemy sessions
# class CRUD():
#     def add(self, resource):
#         db.session.add(resource)
#         return db.session.commit()

#     def update(self):
#         return db.session.commit()

#     def delete(self, resource):
#         db.session.delete(resource)
#         return db.session.commit()
