from app import db
from api.v1.menus.models import Menus
from sqlalchemy import text

def seed_menus():
    db.session.query(Menus).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE menus AUTO_INCREMENT = 1"))
    menus = [
        Menus(
            parent_id =0,
            permission_id =1,
            name='dashboard',
            display_name='Dashboard',
            url='home',
            icon ='fa fa-home text-primary',
            seq = 1,
        ),
        Menus(
            parent_id =0,
            permission_id =2,
            name='administrator',
            display_name='Administrator',
            url='',
            icon ='fa fa-gavel text-primary',
            seq = 2,
        ),
        Menus(
            parent_id =2,
            permission_id =2,
            name='manage-user',
            display_name='Manage User',
            url='acl/user',
            icon ='',
            seq = 1,
        ),
        Menus(
            parent_id =2,
            permission_id =10,
            name='manage-menu',
            display_name='Manage Menu',
            url='acl/menu',
            icon ='',
            seq = 2,
        ),
        Menus(
            parent_id =2,
            permission_id =6,
            name='manage-role',
            display_name='Manage Role',
            url='acl/role',
            icon ='',
            seq = 3,
        ),
        Menus(
            parent_id =2,
            permission_id =13,
            name='manage-authorization',
            display_name='Manage Authorization',
            url='acl/authorization',
            icon ='',
            seq = 4,
        ),
        Menus(
            parent_id =2,
            permission_id =10,
            name='manage-module',
            display_name='Manage Module',
            url='acl/module',
            icon ='',
            seq = 5,
        ),

    ]

    db.session.bulk_save_objects(menus)
    db.session.commit()
