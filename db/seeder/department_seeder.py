from app import db
from api.v1.department.models import M_department
from sqlalchemy import text

def seed_department():
    db.session.query(M_department).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE m_department AUTO_INCREMENT = 1"))
    department = [
        M_department(
            code='EXCTV',
            name='Executive',
            division_id=1,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='NEWS',
            name='News',
            division_id=2,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='NEWS1',
            name='News 1',
            division_id=2,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='NEWS2',
            name='News 2',
            division_id=2,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='NEWSOP',
            name='News Operation',
            division_id=2,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='SPRT',
            name='Support',
            division_id=3,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='TECHNIC',
            name='Technic',
            division_id=3,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='OPRTN',
            name='Operation',
            division_id=4,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='PRGDIGI',
            name='Programming & Digital',
            division_id=4,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='HRGA',
            name='HR & GA',
            division_id=4,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='FNCE',
            name='Finance',
            division_id=4,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_department(
            code='SLSMARK',
            name='Sales & Marketing',
            division_id=4,
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        )
    ]

    db.session.bulk_save_objects(department)
    db.session.commit()
