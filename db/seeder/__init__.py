from .user_seeder import seed_user
from .division_seeder import seed_division
from .department_seeder import seed_department
from .position_seeder import seed_position
from .roles_seeder import seed_roles
from .modules_seeder import seed_modules
from .permissions_seeder import seed_permissions
from .permission_role_seeder import seed_permission_role
from .menus_seeder import seed_menus

def run_seeding():
    seed_user()
    seed_division()
    seed_department()
    seed_position()
    seed_roles()
    seed_modules()
    seed_permissions()
    seed_permission_role()
    seed_menus()
