from app import db
from api.v1.roles.models import Roles
from sqlalchemy import text
from datetime import datetime

def seed_roles():
    db.session.query(Roles).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE roles AUTO_INCREMENT = 1"))
    roles = [
        Roles(
            name='super-admin',
            display_name='Super Admin',
            description='All Access'
        ),
        Roles(
            name='admin-mcr',
            display_name='Admin MCR',
            description='Admin MCR'
        ),
        Roles(
            name='user',
            display_name='User',
            description='user'
        )
    ]

    db.session.bulk_save_objects(roles)
    db.session.commit()
