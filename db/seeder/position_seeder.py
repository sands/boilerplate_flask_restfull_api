from app import db
from api.v1.position.models import M_position
from sqlalchemy import text

def seed_position():
    db.session.query(M_position).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE m_position AUTO_INCREMENT = 1"))
    post = [
        M_position(
            code='CLV',
            name='C-Level',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_position(
            code='GMG',
            name='General Manager',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_position(
            code='MNG',
            name='Manager',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_position(
            code='SPV',
            name='Supervisor',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_position(
            code='STF',
            name='Staff',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
    ]

    db.session.bulk_save_objects(post)
    db.session.commit()
