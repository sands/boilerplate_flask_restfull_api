from app import db
from api.v1.modules.models import Modules
from sqlalchemy import text

def seed_modules():
    db.session.query(Modules).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE modules AUTO_INCREMENT = 1"))
    modules = [
        Modules(
            name='dashboard',
            display_name='Dashboard',
            description='Dashboard',
            status='1',
        ),
        Modules(
            name='user',
            display_name='User',
            description='User Management',
            status='1',
        ),
        Modules(
            name='role',
            display_name='Role',
            description='Role Management',
            status='1',
        ),
        Modules(
            name='permission',
            display_name='Permission',
            description='Manage Permission',
            status='1',
        ),
        Modules(
            name='authorization',
            display_name='Authorization',
            description='Manage Authorization, Menu, Module',
            status='1',
        )
    ]

    db.session.bulk_save_objects(modules)
    db.session.commit()
