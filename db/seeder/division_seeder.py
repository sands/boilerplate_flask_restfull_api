from app import db
from api.v1.division.models import M_division
from sqlalchemy import text

def seed_division():
    db.session.query(M_division).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE m_division AUTO_INCREMENT = 1"))
    division = [
        M_division(
            code='EXC',
            name='Executive',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_division(
            code='NWS',
            name='News',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_division(
            code='TEC',
            name='Technic',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
        M_division(
            code='OPR',
            name='Operation',
            description='',
            status='1',
            created_by=1,
            updated_by=1,
        ),
    ]

    db.session.bulk_save_objects(division)
    db.session.commit()
