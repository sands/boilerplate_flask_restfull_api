from app import db
from api.v1.permissions_role.models import Permissions_role
from werkzeug.security import generate_password_hash
from sqlalchemy import text

def seed_permission_role():
    db.session.query(Permissions_role).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    # db.session.execute(text("ALTER TABLE permission_role AUTO_INCREMENT = 1"))
    permission_role = [
        Permissions_role(
            permission_id=1,
            role_id=1,
        ),
        Permissions_role(
            permission_id=2,
            role_id=1,
        ),
        Permissions_role(
            permission_id=3,
            role_id=1,
        ),
        Permissions_role(
            permission_id=4,
            role_id=1,
        ),
        Permissions_role(
            permission_id=5,
            role_id=1,
        ),
        Permissions_role(
            permission_id=6,
            role_id=1,
        ),
        Permissions_role(
            permission_id=7,
            role_id=1,
        ),
        Permissions_role(
            permission_id=8,
            role_id=1,
        ),
        Permissions_role(
            permission_id=9,
            role_id=1,
        ),
        Permissions_role(
            permission_id=10,
            role_id=1,
        ),
        Permissions_role(
            permission_id=11,
            role_id=1,
        ),
        Permissions_role(
            permission_id=12,
            role_id=1,
        ),
        Permissions_role(
            permission_id=13,
            role_id=1,
        ),
        Permissions_role(
            permission_id=14,
            role_id=1,
        ),
        Permissions_role(
            permission_id=15,
            role_id=1,
        ),
        Permissions_role(
            permission_id=16,
            role_id=1,
        ),
        Permissions_role(
            permission_id=17,
            role_id=1,
        ),
    ]

    db.session.bulk_save_objects(permission_role)
    db.session.commit()

