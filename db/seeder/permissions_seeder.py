from app import db
from api.v1.permissions.models import Permissions
from sqlalchemy import text


def seed_permissions():
    db.session.query(Permissions).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE permissions AUTO_INCREMENT = 1"))
    permissions = [
        Permissions(
            name="show-dashboard",
            display_name="Show Dashboard",
            description="Show Dashboard",
            module_id=1,
        ),
        Permissions(
            name="show-user",
            display_name="Show User",
            description="Show User",
            module_id=2,
        ),
        Permissions(
            name="create-user",
            display_name="Create User",
            description="Create User",
            module_id=2,
        ),
        Permissions(
            name="update-user",
            display_name="Update User",
            description="Update User",
            module_id=2,
        ),
        Permissions(
            name="delete-user",
            display_name="Delete User",
            description="Delete User",
            module_id=2,
        ),
        Permissions(
            name="show-role",
            display_name="Show Role",
            description="Show Role",
            module_id=3,
        ),
        Permissions(
            name="create-role",
            display_name="Create Role",
            description="Create Role",
            module_id=3,
        ),
        Permissions(
            name="update-role",
            display_name="Update Role",
            description="Update Role",
            module_id=3,
        ),
        Permissions(
            name="delete-role",
            display_name="Delete Role",
            description="Delete Role",
            module_id=3,
        ),
        Permissions(
            name="show-permission",
            display_name="Show Permission",
            description="Show Permission",
            module_id=4,
        ),
        Permissions(
            name="create-permission",
            display_name="Create Permission",
            description="Create Permission",
            module_id=4,
        ),
        Permissions(
            name="delete-permission",
            display_name="Delete Permission",
            description="Delete Permission",
            module_id=4,
        ),
        Permissions(
            name="show-authorization",
            display_name="Show Authorization",
            description="Show Authorization",
            module_id=5,
        ),
        Permissions(
            name="create-authorization",
            display_name="Create Authorization",
            description="Create Authorization",
            module_id=5,
        ),
        Permissions(
            name="update-authorization",
            display_name="Update Authorization",
            description="Update Authorization",
            module_id=5,
        ),
        Permissions(
            name="update-authorization",
            display_name="Update Authorization",
            description="Update Authorization",
            module_id=5,
        ),
        Permissions(
            name="delete-authorization",
            display_name="Delete Authorization",
            description="Delete Authorization",
            module_id=5,
        ),
    ]

    db.session.bulk_save_objects(permissions)
    db.session.commit()
