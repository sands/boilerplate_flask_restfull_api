from app import db
from api.v1.user.models import User
from werkzeug.security import generate_password_hash
from sqlalchemy import text

def seed_user():
    db.session.query(User).delete()
    # Reset the auto-increment value for the 'id' column in MySQL
    db.session.execute(text("ALTER TABLE user AUTO_INCREMENT = 1"))
    user = [
        User(
            fullname='Admin',
            username='admin',
            email='admin@gmail.com',
            password='123456789',  # Use the password attribute directly
            status= '1',
            created_by=1,
            updated_by=1,
        )
    ]

    db.session.bulk_save_objects(user)
    db.session.commit()

