# class User(db.Model):
#     __tablename__ = 'users'
#     id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     name = db.Column(db.String(255))
#     city = db.Column(db.String(255))
#     state = db.Column(db.String(255))
#     postal = db.Column(db.Integer)
#     password = db.Column(db.String(255), default="password")

#     def __init__(self, name, city, state, postal, password="password"):
#         self.name = name
#         self.city = city
#         self.state = state
#         self.postal = postal
#         self.password = password

#     def __repr__(self):
#         return f"User(name={self.name}, city={self.city}, state={self.state}, postal={self.postal})"

#     def to_dict(self):
#         return {
#             "id": self.id,
#             "name": self.name,
#             "city": self.city,
#             "state": self.state,
#             "postal": self.postal,
#             "password": self.password
#         }


# class Product(db.Model):
#     __tablename__ = 'products'
#     id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     name = db.Column(db.String(255))
#     category = db.Column(db.String(255))
#     sub_category = db.Column(db.String(255))

#     def __init__(self, name, category, sub_category):
#         self.name = name
#         self.category = category
#         self.sub_category = sub_category

#     def __repr__(self):
#         return f"Product(name={self.name}, category={self.category}, sub_category={self.sub_category})"

#     def to_dict(self):
#         return {
#             "id": self.id,
#             "name": self.name,
#             "category": self.category,
#             "sub_category": self.sub_category
#         }

# Import Models
from api.v1.division.models import M_division
from api.v1.department.models import M_department
from api.v1.unit.models import M_unit
from api.v1.role_job.models import M_role_job
from api.v1.position.models import M_position
from api.v1.roles.models import Roles
from api.v1.menus.models import Menus
from api.v1.user.models import User
from api.v1.role_user.models import Role_user
from api.v1.permissions.models import Permissions
from api.v1.permissions_role.models import Permissions_role
from api.v1.modules.models import Modules
from api.v1.adr_error_type.models import Adr_error_type
from api.v1.adr_evidence.models import Adr_evidence
from api.v1.adr_operators.models import Adr_operators
from api.v1.adr_status.models import Adr_status
from api.v1.air_deviation.models import Air_deviation
