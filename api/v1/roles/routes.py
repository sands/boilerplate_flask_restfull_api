from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import jwt_required
from flask import request

role_ns = Namespace("role", description="Master Roles User")


@role_ns.route("/all")
class Roles(Resource):
    @role_ns.header("Authorization", "JWT Token", required=True)
    @role_ns.doc(
        summary="Get all role in the database (admin only)",
        description="This endpoint returns a list of all role in the database. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all role in the database (admin only)"""
        return get_roles()


@role_ns.route("/get/<int:role_id>")
class RoleById(Resource):
    @role_ns.header("Authorization" "JWT Token", required=True)
    @role_ns.doc(
        summary="Get role in the database",
        description="This endpoint return role data in the database.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, role_id):
        """Get a role by id (admin only)"""
        return get_role(role_id)


@role_ns.route("/add")
class AddRole(Resource):
    @role_ns.header("Authorization" "JWT Token", required=True)
    @role_ns.doc(
        summary="Add a Role (admin only)",
        description="This endpoint allows you to add a Role. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, display_name, and description. The id will be automatically generated. "
                "provided.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "display_name": {"type": "string"},
                        "description": {"type": "string"},
                    },
                    "required": [
                        "name",
                        "display_name",
                        "description",
                    ],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        """Add a new Role (admin only)"""
        data = request.get_json()
        return add_role(data)


@role_ns.route("/edit/<int:role_id>")
class EditRole(Resource):
    @role_ns.header("Authorization", "JWT Token", required=True)
    @role_ns.doc(
        summary="Update a role (admin only)",
        description="This endpoint allows you to update a role. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input your name, display_name and description."
                "Only the fields you want to update are required.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "display_name": {"type": "string"},
                        "description": {"type": "string"},
                    },
                },
            },
        },
    )
    @jwt_required()
    def put(self, role_id):
        """Edit a role (admin only)"""
        data = request.get_json()
        return update(data, role_id)


@role_ns.route("/delete/<int:role_id>")
class DeleteRole(Resource):
    @role_ns.header("Authorization", "JWT Token", required=True)
    @role_ns.doc(
        summary="Delete an role",
        description="This endpoint allows you to delete an role. It is restricted to admins",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, role_id):
        """Delete role by id for admin"""
        return delete(role_id)


@role_ns.route("/datatable")
class DatatableRole(Resource):
    @role_ns.header("Authorization", "JWT Token", required=True)
    @role_ns.doc(
        summary="Get all roles in the database with datatable format (admin only)",
        description="This endpoint returns a list of all roles in the database with datatable format. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data Role in Datatable Format"""
        return datatable(request.args)
