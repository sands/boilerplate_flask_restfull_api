from flask import jsonify
from .models import *
from ..user.controllers import is_admin
from app import db
import sys

sys.path.append("..")
from api.v1.common.response import api_response


def get_role(role_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        role = Roles.query.filter_by(id=role_id).first()
        if role:
            role = role.to_dict()
            return api_response(
                "success", "Role retrieved successfully", role, status_code
            )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def get_roles():
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        roles = Roles.query.all()
        datas = [e.to_dict() for e in roles]
        return api_response(
            "success", "Roles retrieved successfully", datas, status_code
        )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def add_role(data):
    try:
        form = RoleAddForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            display_name = data.get("display_name")
            description = data.get("description")
            role_check = Roles.query.filter_by(name=name).first()

            if role_check:
                status_code = 400
                return api_response("error", "Role Taken", response_code=status_code)
            last_role_item = Roles.query.order_by(Roles.id.desc()).first()
            new_role_id = last_role_item.id + 1 if last_role_item is not None else 1
            role = Roles(
                id=new_role_id,
                name=name,
                display_name=display_name,
                description=description,
            )

            db.session.add(role)
            db.session.commit()

            return api_response(
                "success", "Add role successfully", role.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def update(data, role_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit user", response_code=status_code
            )

        form = RoleUpdateForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            display_name = data.get("display_name")
            description = data.get("description")

            role = Roles.query.filter_by(id=role_id).first()

            if role:
                if "name" in data:
                    if Roles.query.filter_by(name=name).first():
                        return api_response("error", "name taken", response_code=400)
                    role.name = name
                if "display_name" in data:
                    role.display_name = display_name
                if "description" in data:
                    role.description = description
            else:
                return api_response("error", "Role Not Found", response_code=400)

            db.session.commit()

            return api_response(
                "success", "Update role successfully", role.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def delete(role_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete role", response_code=status_code
            )

        role = Roles.query.filter_by(id=role_id).first()

        if role:
            status_code = 200

            # Delete Role
            db.session.delete(role)
            db.session.commit()

            return api_response(
                "success", "Delete Role successfully", role.to_dict(), status_code
            )
        else:
            return api_response("error", "Role Not Found", response_code=400)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        role = Roles.query

        if search_value:
            role = role.filter(Roles.name.like(f"%{search_value}%"))

        role = role.limit(length).offset(start_idx).all()

        # Count the total number of roles
        total_role = Roles.query.count()

        datas = [e.to_dict() for e in role]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_role,
            "recordsFiltered": len(role),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
