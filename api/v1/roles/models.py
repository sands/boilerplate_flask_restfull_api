from app import db
from sqlalchemy import func
from wtforms import Form, StringField
from wtforms.validators import DataRequired, Email
from enum import Enum

ROLE_SUPERADMIN = 1
ROLE_USER = 3


class Roles(db.Model):
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    display_name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=True
    )

    def __repr__(self):
        return f"Roles(name={self.name}, display_name={self.display_name}, description={self.description})"

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "display_name": self.display_name,
            "description": self.description,
        }

    def get_role_superadmin_id():
        return ROLE_SUPERADMIN

    def get_role_user_id():
        return ROLE_USER


class RoleAddForm(Form):
    name = StringField("Name", validators=[DataRequired()])
    display_name = StringField("Display Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])

class RoleUpdateForm(Form):
    name = StringField("Name", validators=[DataRequired()])
