from app import db
from sqlalchemy import func
from wtforms import Form, StringField, validators
from wtforms.validators import *
import json


class Permissions(db.Model):
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    module_id = db.Column(
        db.Integer, db.ForeignKey("modules.id", ondelete="CASCADE"), nullable=False
    )
    name = db.Column(db.String(255), nullable=False)
    display_name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=False
    )

    # Relations
    # module = db.relationship('Modules', backref=('permissions'), lazy=True, cascade='all')

    def __repr__(self):
        return f"name(name={self.name},module_id={self.module_id},display_name={self.display_name}, description={self.description})"

    def to_dict(self):
        return {
            "id": self.id,
            "module_id": self.module_id,
            "name": self.name,
            "display_name": self.display_name,
            "description": self.description,
        }


# Custom JSON encoder class to handle Permissions objects serialization
class PermissionsEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Permissions):
            return {
                "id": obj.id,
                "module_id": obj.module_id,
                "name": obj.name,
                "display_name": obj.display_name,
                "description": obj.description,
            }
        return super().default(obj)


class PermissionAddByModuleForm(Form):
    name = StringField("name", validators=[DataRequired()])
    display_name = StringField("Display Name", validators=[DataRequired()])
    description = StringField("Description", validators=[Optional()])
