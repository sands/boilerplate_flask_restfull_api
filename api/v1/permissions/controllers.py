from flask import request, jsonify, redirect
from .models import Permissions, PermissionAddByModuleForm, PermissionsEncoder
from app import app, db
from flask_jwt_extended import create_access_token, jwt_required
from app import app, db
import jwt
import datetime
import json
from datetime import timedelta
import sys

sys.path.append("..")
from api.v1.common.response import api_response
from api.v1.user.controllers import is_admin


def add_by_module(data, module_id):
    try:
        status_code = 200
        messages = 'Permissions successfully added'
        if(module_id is None or module_id == "" or module_id ==0):
            status_code = 400
            return json.dumps({'status':status_code, 'messages':"Invalid Module ID"})
        inserted_data = []  # Create a list to store the inserted permission data
        for value in data:
            form = PermissionAddByModuleForm(data=value)
            if form.validate():
                permission = Permissions(
                    module_id = module_id,
                    name=value.get('name'),
                    display_name = value.get('display_name'),
                    description = value.get('description')
                )
                db.session.add(permission)
                inserted_data.append(permission)  # Append the inserted permission to the list
            else:
                status_code = 400
                messages = "Invalid Permissions Data"
        return json.dumps({'status':status_code, 'messages':messages, 'data':inserted_data}, cls=PermissionsEncoder)
    except Exception as e:
        status_code = 500
        return json.dumps({'status':status_code, 'messages':str(e)})
