from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import create_access_token, jwt_required
from flask import request

department_ns = Namespace("department", description="M_Department")

@department_ns.route("/get-by-code/<string:department_code>")
class getDepartmentByCode(Resource):
    @department_ns.header("Authorization" "JWT Token", required=True)
    @department_ns.doc(
        summary="Get a department (admin only)",
        description="This endpoint allows you to get a department. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, department_code):
        # Get a department (admin only)
        return get_by_code(department_code)

@department_ns.route("/get/<int:department_id>")
class getDepartmentById(Resource):
    @department_ns.header("Authorization" "JWT Token", required=True)
    @department_ns.doc(
        summary="Get a department (admin only)",
        description="This endpoint allows you to get a department. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, department_id):
        # Get a department (admin only)
        return get_by_id(department_id)

@department_ns.route("/all")
class getAllDepartments(Resource):
    @department_ns.header("Authorization", "JWT Token", required=True)
    @department_ns.doc(
        summary="Get all departments in the database (admin only)",
        description="This endpoint returns a list of all departments in the database. It is restricted to admin only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all departments in the database (admin only)"""
        return get_all_department(request.args)
    
@department_ns.route("/add")
class AddDepartment(Resource):
    @department_ns.header("Authorization" "JWT Token", required=True)
    @department_ns.doc(
        summary="Add a department (admin only)",
        description="This endpoint allows you to add a department. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input division_id,code, name, description and status. The id will be automatically generated. "
                "The description is optional",
                "schema": {
                    "type": "object",
                    "properties": {
                        "division_id": {"type": "int"},
                        "code": {"type": "string"},
                        "name": {"type": "string"},
                        "description": {"type": "string"},
                        "status": {"type": "int"},
                    },
                    "required": ["division_id","code","name", "status"],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        # Add a department (admin only)
        data = request.get_json()
        return add(data)
    
@department_ns.route("/edit/<int:department_id>")
class UpdateDepartment(Resource):
    @department_ns.header("Authorization" "JWT Token", required=True)
    @department_ns.doc(
        summary="Update a department (admin only)",
        description="This endpoint allows you to Update a department. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input division_id,code, name, description and status. The id will be automatically generated. "
                "The description is optional",
                "schema": {
                    "type": "object",
                    "properties": {
                        "division_id": {"type": "int"},
                        "code": {"type": "string"},
                        "name": {"type": "string"},
                        "description": {"type": "string"},
                        "status": {"type": "int"},
                    },
                    "required": ["division_id","code","name", "status"],
                },
            },
        },
    )
    @jwt_required()
    def post(self, department_id):
        # Update a department (admin only)
        data = request.get_json()
        return update(data, department_id)

@department_ns.route("/delete/<int:department_id>")
class DeleteDepartment(Resource):
    @department_ns.header("Authorization" "JWT Token", required=True)
    @department_ns.doc(
        summary="Delete a department (admin only)",
        description="This endpoint allows you to delete a department. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, department_id):
        # Delete a department (admin only)
        return delete(department_id)
    
@department_ns.route("/datatable")
class DatatableDepartment(Resource):
    @department_ns.header("Authorization", "JWT Token", required=True)
    @department_ns.doc(
        summary="Get all departments in the database with datatable format (admin only)",
        description="This endpoint returns a list of all departments in the database with datatable format. It is restricted to admin only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data departments in Datatable Format"""
        return datatable(request.args)