from flask import request, jsonify, redirect
from .models import M_department, DepartmentAddForm, DepartmentUpdateForm
from ..user.controllers import get_user_id_from_token
from api.v1.user.controllers import is_admin
from app import app, db
from flask_jwt_extended import create_access_token, jwt_required
from sqlalchemy.orm import joinedload
from sqlalchemy import or_
import jwt
import json
from datetime import timedelta
import sys

sys.path.append("..")
from api.v1.common.response import api_response

def get_by_code(department_code):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)
        
        department = M_department.query.filter_by(code=department_code).first()
        if department:
            return api_response(
                "success", "Get a department successfully", department.to_dict(), status_code
            )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def get_by_id(department_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)
        
        department = M_department.query.filter_by(id=department_id).first()
        if department:
            return api_response(
                "success", "Get a department successfully", department.to_dict(), status_code
            )
        else:
            status_code = 400
            return api_response("error", "Department Not Found", response_code=status_code)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)

def get_all_department(data):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        default_page = 1
        default_per_page = 10

        page = int(data.get("page", default_page))
        per_page = int(data.get("per_page", default_per_page))

        # Calculate the offset based on the page number and items per page
        offset = (page - 1) * per_page

        departments = M_department.query
        search_name = data.get("search_name").strip() if data.get("search_name") else ""
        search_code = (
            data.get("search_code") if data.get("search_code") else ""
        )
        
        # Apply filters for search parameters if provided
        if search_name:
            departments = departments.filter(M_department.name.like(f"%{search_name}%"))
        if search_code:
            departments = departments.filter(M_department.code.like(f"%{search_code}%"))

        departments = departments.offset(offset).limit(per_page).all()

        datas = [e.to_dict() for e in departments]
        return api_response(
            "success", "Departments retrieved successfully", datas, status_code
        )

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)

def add(data):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit department", response_code=status_code
            )

        form = DepartmentAddForm(data=data)
        if form.validate():
            status_code = 200

            division_id= data.get("division_id")
            code= data.get("code").strip()
            name = data.get("name").strip()
            description = data.get("description").strip()
            status = data.get("status")
            user_id = get_user_id_from_token()

            departments = M_department(
                division_id=division_id,
                code=code,
                name=name,
                description=description,
                status=status,
                created_by = user_id,
                updated_by = user_id
            )
            db.session.add(departments)
            db.session.commit()
            return api_response(
                "success",
                "Add department successfully",
                departments.to_dict(),
                status_code,
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def update(data, department_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit department", response_code=status_code
            )
        
        # return data, department_id
        form = DepartmentUpdateForm(data=data, department_id=department_id)
        # return form.validate()
        if form.validate():
            # return "hello"
            status_code = 200

            division_id = data.get("division_id")
            code= data.get("code").strip()
            name = data.get("name").strip()
            description = data.get("description").strip()
            status = data.get("status")
            user_id = get_user_id_from_token()

            # Get Divisi
            department = M_department.query.get(department_id)
            if department:
                department.division_id = division_id
                department.code = code
                department.name = name
                department.description = description
                department.status = status
                department.updated_by = user_id

                db.session.commit()
                return api_response(
                        "success",
                        "Update department successfully",
                        department.to_dict(),
                        status_code,
                    )
            else:
                status_code = 400
                return api_response(
                    "error", "Department Not Found", response_code=status_code
                )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)

def delete(department_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete department", response_code=status_code
            )

        department = M_department.query.filter_by(id=department_id).first()

        if department:
            status_code = 200
            # Delete department
            db.session.delete(department)

            db.session.commit()
            return api_response(
                "success", "Delete department successfully", department.to_dict(), status_code
            )
        else:
            return api_response("error", "department Not Found", response_code=400)
    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        departments = M_department.query

        if search_value:
            departments = departments.filter(or_(
                M_department.name.like(f"%{search_value}%"),
                M_department.code.like(f"%{search_value}%")
            ))

        departments = departments.limit(length).offset(start_idx).all()

        # Count the total number of departments
        total_departments = M_department.query.count()

        datas = [e.to_dict() for e in departments]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_departments,
            "recordsFiltered": len(departments),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)