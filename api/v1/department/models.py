from app import db
from sqlalchemy import func
from wtforms import Form, StringField, validators, IntegerField
from wtforms.validators import *
from ..division.models import M_division
import json


class M_department(db.Model):
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.String(255), nullable=False)
    division_id = db.Column(
        db.Integer, db.ForeignKey("m_division.id", ondelete="CASCADE"), nullable=False
    )
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=True)
    status = db.Column(db.Enum("0", "1"), default="0", nullable=False)
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=False
    )
    created_by = db.Column(db.Integer, nullable=False)
    updated_by = db.Column(db.Integer, nullable=False)

    # Relations
    role_job = db.relationship(
        "M_role_job", cascade="all, delete-orphan", backref="department", lazy=True
    )
    unit=db.relationship(
        "M_unit", cascade="all, delete-orphan", backref="department", lazy=True
    )
    # division = db.relationship('M_division', foreign_keys=[division_id], backref=db.backref('department', lazy=True))
    # created_by_user = db.relationship('User', foreign_keys=[created_by], backref=db.backref('created_departments', lazy=True))
    # updated_by_user = db.relationship('User', foreign_keys=[updated_by], backref=db.backref('updated_departments', lazy=True))

    def __repr__(self):
        return f"M_department(code={self.code},division_id={self.division_id},name={self.name}, description={self.description}, status={self.status})"

    def to_dict(self):
        return {
            "id":self.id,
            "code": self.code,
            "division_id": self.division_id,
            "name": self.name,
            "description": self.description,
            "status": self.status,
            "role_jobs":[role_jobs.to_dict() for role_jobs in self.role_job],
            "unit":[units.to_dict() for units in self.unit],
        }


class CodeExistsValidator(object):
    def __init__(self, message=None):
        if not message:
            message = "This department code already exists."
        self.message = message

    def __call__(self, form, field):
        code = (
            field.data.strip()
        )  # Assuming the code is a string and should be stripped of any extra spaces
        existing_department = M_department.query.filter_by(code=code).first()
        if existing_department:
            raise ValidationError(self.message)


class NameExistsValidator(object):
    def __init__(self, message=None):
        if not message:
            message = "This department name already exists."
        self.message = message

    def __call__(self, form, field):
        name = (
            field.data.strip()
        )  # Assuming the name is a string and should be stripped of any extra spaces
        division_id = form.division_id.data  # Get the division_id from the form
        existing_department = M_department.query.filter_by(name=name, division_id=division_id).first()
        if existing_department:
            raise ValidationError(self.message)


class DivisionExistsValidator(object):
    def __init__(self, message=None):
        if not message:
            message = "Invalid Division ID"
        self.message = message

    def __call__(self, form, field):
        division_id = field.data
        division = M_division.query.filter_by(id=division_id).first()
        if not division:
            raise ValidationError(self.message)
        
class CodeExistsUpdateValidator(object):
    def __init__(self, department_id, message=None):
        if not message:
            message = "This code already exists."
        self.message = message
        self.department_id = department_id

    def __call__(self, form, field):
        code = (
            field.data.strip()
        )  # Assuming the code is a string and should be stripped of any extra spaces
        existing_department = M_department.query.filter(
            M_department.id != self.department_id, M_department.code == code
        ).first()
        if existing_department:
            raise ValidationError(self.message)

class   NameExistsUpdateValidator(object):
    def __init__(self, department_id, message=None):
        if not message:
            message = "This name already exists."
        self.message = message
        self.department_id = department_id

    def __call__(self, form, field):
        name = (
            field.data.strip()
        )  # Assuming the name is a string and should be stripped of any extra spaces
        division_id = form.division_id.data  # Get the division_id from the form
        existing_department = M_department.query.filter(M_department.name==name, M_department.division_id==division_id, M_department.id != self.department_id).first()
        if existing_department:
            raise ValidationError(self.message)


class DepartmentAddForm(Form):
    division_id = IntegerField(
        "division_id",
        validators=[DataRequired(), DivisionExistsValidator()],
    )

    code = StringField(
        "code",
        validators=[
            DataRequired(),
            CodeExistsValidator(),
            validators.Length(max=255),
        ],
    )
    name = StringField(
        "name",
        validators=[DataRequired(), validators.Length(max=255), NameExistsValidator()],
    )
    description = StringField("Description", validators=[Optional()])
    status = StringField("Status", validators=[DataRequired()])


class DepartmentUpdateForm(Form):
    division_id = IntegerField(
        "Division Id",
        validators=[DataRequired(), DivisionExistsValidator()],
    )
    code = StringField(
        "code",
        validators=[
            DataRequired(),
            validators.Length(max=255),
        ],
    )
    name = StringField("name", validators=[DataRequired(), validators.Length(max=255)])
    description = StringField("Description", validators=[Optional()])
    status = StringField("Status", validators=[DataRequired()])


    def __init__(self, department_id, *args, **kwargs):
        super(DepartmentUpdateForm, self).__init__(*args, **kwargs)
        self.department_id = department_id

        # Add custom validators to the fields
        self._add_code_validator()
        self._add_name_validator()

    def _add_code_validator(self):
        if not any(isinstance(validator, CodeExistsUpdateValidator) for validator in self.code.validators):
            self.code.validators.append(CodeExistsUpdateValidator(department_id=self.department_id))

    def _add_name_validator(self):
        if not any(isinstance(validator, NameExistsUpdateValidator) for validator in self.name.validators):
            self.name.validators.append(NameExistsUpdateValidator(department_id=self.department_id))
