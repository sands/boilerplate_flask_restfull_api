from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import create_access_token, jwt_required
from flask import request

division_ns = Namespace("division", description="M_division")

@division_ns.route("/get-by-code/<string:division_code>")
class getDivisionByCode(Resource):
    @division_ns.header("Authorization" "JWT Token", required=True)
    @division_ns.doc(
        summary="Get a division (admin only)",
        description="This endpoint allows you to get a division. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, division_code):
        # Get a division (admin only)
        return get_by_code(division_code)

@division_ns.route("/get/<int:division_id>")
class getDivisionById(Resource):
    @division_ns.header("Authorization" "JWT Token", required=True)
    @division_ns.doc(
        summary="Get a division (admin only)",
        description="This endpoint allows you to get a division. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, division_id):
        # Get a division (admin only)
        return get_by_id(division_id)
    
@division_ns.route("/all")
class getAllDivisions(Resource):
    @division_ns.header("Authorization", "JWT Token", required=True)
    @division_ns.doc(
        summary="Get all divisions in the database (admin only)",
        description="This endpoint returns a list of all divisions in the database. It is restricted to admin only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all divisions in the database (admin only)"""
        return get_all_division(request.args)
    
@division_ns.route("/add")
class AddDivision(Resource):
    @division_ns.header("Authorization" "JWT Token", required=True)
    @division_ns.doc(
        summary="Add a division (admin only)",
        description="This endpoint allows you to add a division. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input code, name, description and status. The id will be automatically generated. "
                "The description is optional",
                "schema": {
                    "type": "object",
                    "properties": {
                        "code": {"type": "string"},
                        "name": {"type": "string"},
                        "description": {"type": "string"},
                        "status": {"type": "int"},
                    },
                    "required": ["code","name", "status"],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        # Add a division (admin only)
        data = request.get_json()
        return add(data)
    
@division_ns.route("/edit/<int:division_id>")
class UpdateDivision(Resource):
    @division_ns.header("Authorization" "JWT Token", required=True)
    @division_ns.doc(
        summary="Update a division (admin only)",
        description="This endpoint allows you to Update a division. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input code, name, description and status. The id will be automatically generated. "
                "The description is optional",
                "schema": {
                    "type": "object",
                    "properties": {
                        "code": {"type": "string"},
                        "name": {"type": "string"},
                        "description": {"type": "string"},
                        "status": {"type": "int"},
                    },
                    "required": ["code","name", "status"],
                },
            },
        },
    )
    @jwt_required()
    def post(self, division_id):
        # Update a division (admin only)
        data = request.get_json()
        return update(data, division_id)

@division_ns.route("/delete/<int:division_id>")
class DeleteDivision(Resource):
    @division_ns.header("Authorization" "JWT Token", required=True)
    @division_ns.doc(
        summary="Delete a division (admin only)",
        description="This endpoint allows you to delete a division. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, division_id):
        # Delete a division (admin only)
        return delete(division_id)
    
@division_ns.route("/datatable")
class DatatableDivision(Resource):
    @division_ns.header("Authorization", "JWT Token", required=True)
    @division_ns.doc(
        summary="Get all divisions in the database with datatable format (admin only)",
        description="This endpoint returns a list of all divisions in the database with datatable format. It is restricted to admin only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data Divisions in Datatable Format"""
        return datatable(request.args)

