from app import db
from sqlalchemy import func
from wtforms import Form, StringField, validators
from wtforms.validators import *
import json

DivisionStatusDict = {"active": "1", "not_active": "0"}


class M_division(db.Model):
    __tablename__ = "m_division"
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=True)
    status = db.Column(
        db.Enum(DivisionStatusDict["not_active"], DivisionStatusDict["active"]),
        default=DivisionStatusDict["active"],
    )
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=False
    )
    created_by = db.Column(db.Integer, nullable=False)
    updated_by = db.Column(db.Integer, nullable=False)

    # Relations
    departments = db.relationship(
        "M_department", cascade="all, delete-orphan", backref="division", lazy=True
    )
    # created_by_user = db.relationship('User', foreign_keys=[created_by], backref=db.backref('created_divisions', lazy=True))
    # updated_by_user = db.relationship('User', foreign_keys=[updated_by], backref=db.backref('updated_divisions', lazy=True))

    def __repr__(self):
        return f"M_division(code={self.code},name={self.name}, description={self.description}, status={self.status})"

    def to_dict(self):
        return {
            "id":self.id,
            "code": self.code,
            "name": self.name,
            "description": self.description,
            "status": self.status,
            "departments": [department.to_dict() for department in self.departments],
        }


class CodeExistsValidator(object):
    def __init__(self, message=None):
        if not message:
            message = "This division code already exists."
        self.message = message

    def __call__(self, form, field):
        code = (
            field.data.strip()
        )  # Assuming the code is a string and should be stripped of any extra spaces
        existing_division = M_division.query.filter_by(code=code).first()
        if existing_division:
            raise ValidationError(self.message)


class CodeExistsUpdateValidator(object):
    def __init__(self, division_id, message=None):
        if not message:
            message = "This code already exists."
        self.message = message
        self.division_id = division_id

    def __call__(self, form, field):
        code = (
            field.data.strip()
        )  # Assuming the code is a string and should be stripped of any extra spaces
        existing_division = M_division.query.filter(
            M_division.id != self.division_id, M_division.code == code
        ).first()
        if existing_division:
            if 'code' in form.errors:
                del form.errors['code']
            raise ValidationError(self.message)


class NameExistsValidator(object):
    def __init__(self, message=None):
        if not message:
            message = "This division name already exists."
        self.message = message

    def __call__(self, form, field):
        name = (
            field.data.strip()
        )  # Assuming the name is a string and should be stripped of any extra spaces
        existing_division = M_division.query.filter_by(name=name).first()
        if existing_division:
            raise ValidationError(self.message)


class NameExistsUpdateValidator(object):
    def __init__(self, division_id, message=None):
        if not message:
            message = "This name already exists."
        self.message = message
        self.division_id = division_id

    def __call__(self, form, field):
        name = (
            field.data.strip()
        )  # Assuming the name is a string and should be stripped of any extra spaces
        existing_division = M_division.query.filter(
            M_division.id != self.division_id, M_division.name == name
        ).first()
        if existing_division:
            raise ValidationError(self.message)


class DivisionExistsValidator(object):
    def __init__(self, message=None):
        if not message:
            message = "This division ID is not valid."
        self.message = message

    def __call__(self, form, field):
        division_id = (
            field.data.strip()
        )  # Assuming the division_id is a string and should be stripped of any extra spaces
        existing_division = M_division.query.filter_by(id=division_id).first()
        if not existing_division:
            raise ValidationError(self.message)


class DivisionAddForm(Form):
    code = StringField(
        "code",
        validators=[
            DataRequired(),
            CodeExistsValidator(),
            validators.Length(max=255),
        ],
    )
    name = StringField(
        "name",
        validators=[DataRequired(), validators.Length(max=255), NameExistsValidator()],
    )
    description = StringField("Description", validators=[Optional()])
    status = StringField("Status", validators=[DataRequired()])


class DivisionUpdateForm(Form):
    code = StringField(
        "code",
        validators=[
            validators.DataRequired(),
            validators.Length(max=255),
        ],
    )
    name = StringField("name", validators=[validators.DataRequired(), validators.Length(max=255)])
    description = StringField("Description", validators=[validators.Optional()])
    status = StringField("Status", validators=[validators.DataRequired()])

    def __init__(self, division_id, *args, **kwargs):
        super(DivisionUpdateForm, self).__init__(*args, **kwargs)
        self.division_id = division_id

        # Add custom validators to the fields
        self._add_code_validator()
        self._add_name_validator()

    def _add_code_validator(self):
        if not any(isinstance(validator, CodeExistsUpdateValidator) for validator in self.code.validators):
            self.code.validators.append(CodeExistsUpdateValidator(division_id=self.division_id))

    def _add_name_validator(self):
        if not any(isinstance(validator, NameExistsUpdateValidator) for validator in self.name.validators):
            self.name.validators.append(NameExistsUpdateValidator(division_id=self.division_id))
