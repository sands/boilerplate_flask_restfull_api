from flask import request, jsonify, redirect
from .models import M_division, DivisionAddForm, DivisionUpdateForm
from ..user.controllers import get_user_id_from_token
from app import app, db
from flask_jwt_extended import create_access_token, jwt_required
from sqlalchemy.orm import joinedload
from sqlalchemy import or_
import jwt
import json
from datetime import timedelta
import sys

sys.path.append("..")
from api.v1.common.response import api_response
from api.v1.user.controllers import is_admin

def get_by_code(division_code):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)
        
        division = M_division.query.filter_by(code=division_code).first()
        if division:
            return api_response(
                "success", "Get a division successfully", division.to_dict(), status_code
            )
        else:
            status_code = 400
            return api_response("error", "Division Not Found", response_code=status_code)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def get_by_id(division_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)
        
        division = M_division.query.filter_by(id=division_id).first()
        if division:
            return api_response(
                "success", "Get a division successfully", division.to_dict(), status_code
            )
        else:
            status_code = 400
            return api_response("error", "Division Not Found", response_code=status_code)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def get_all_division(data):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        default_page = 1
        default_per_page = 10

        page = int(data.get("page", default_page))
        per_page = int(data.get("per_page", default_per_page))

        # Calculate the offset based on the page number and items per page
        offset = (page - 1) * per_page

        divisions = M_division.query
        search_name = data.get("search_name").strip() if data.get("search_name") else ""
        search_code = (
            data.get("search_code") if data.get("search_code") else ""
        )
        
        # Apply filters for search parameters if provided
        if search_name:
            divisions = divisions.filter(M_division.name.like(f"%{search_name}%"))
        if search_code:
            divisions = divisions.filter(M_division.code.like(f"%{search_code}%"))

        divisions = divisions.offset(offset).limit(per_page).all()

        datas = [e.to_dict() for e in divisions]
        return api_response(
            "success", "Divisions retrieved successfully", datas, status_code
        )

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def add(data):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit division", response_code=status_code
            )

        form = DivisionAddForm(data=data)
        if form.validate():
            status_code = 200

            code= data.get("code").strip()
            name = data.get("name").strip()
            description = data.get("description").strip()
            status = data.get("status")
            user_id = get_user_id_from_token()

            divisions = M_division(
                code=code,
                name=name,
                description=description,
                status=status,
                created_by = user_id,
                updated_by = user_id
            )
            db.session.add(divisions)
            db.session.commit()
            return api_response(
                "success",
                "Add divisions successfully",
                divisions.to_dict(),
                status_code,
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def update(data, division_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit division", response_code=status_code
            )
        form = DivisionUpdateForm(data=data, division_id=division_id)
        if form.validate():
            status_code = 200

            code= data.get("code").strip()
            name = data.get("name").strip()
            description = data.get("description").strip()
            status = data.get("status")
            user_id = get_user_id_from_token()

            # Get Divisi
            division = M_division.query.get(division_id)
            if division:
                division.code = code
                division.name = name
                division.description = description
                division.status = status
                division.updated_by = user_id

                db.session.commit()
                return api_response(
                        "success",
                        "Update division successfully",
                        division.to_dict(),
                        status_code,
                    )
            else:
                status_code = 400
                return api_response(
                    "error", "Division Not Found", response_code=status_code
                )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def delete(division_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete division", response_code=status_code
            )

        division = M_division.query.filter_by(id=division_id).first()

        if division:
            status_code = 200
            # Delete Division
            db.session.delete(division)

            db.session.commit()
            return api_response(
                "success", "Delete division successfully", division.to_dict(), status_code
            )
        else:
            return api_response("error", "Division Not Found", response_code=400)
    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        divisions = M_division.query

        if search_value:
            divisions = divisions.filter(or_(
                M_division.name.like(f"%{search_value}%"),
                M_division.code.like(f"%{search_value}%")
            ))

        divisions = divisions.limit(length).offset(start_idx).all()

        # Count the total number of divisions
        total_divisions = M_division.query.count()

        datas = [e.to_dict() for e in divisions]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_divisions,
            "recordsFiltered": len(divisions),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)