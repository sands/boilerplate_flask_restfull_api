from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import create_access_token, jwt_required
from flask import request

module_ns = Namespace("module", description="Modules")


@module_ns.route("/get/<int:module_id>")
class getModule(Resource):
    @module_ns.header("Authorization" "JWT Token", required=True)
    @module_ns.doc(
        summary="Get a module (admin only)",
        description="This endpoint allows you to get a module. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, module_id):
        # Get a module (admin only)
        return get_module(module_id)
    
@module_ns.route("/all")
class getAllModules(Resource):
    @module_ns.header("Authorization", "JWT Token", required=True)
    @module_ns.doc(
        summary="Get all modules in the database (admin only)",
        description="This endpoint returns a list of all modules in the database. It is restricted to admin only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all modules in the database (admin only)"""
        return get_all_module(request.args)


@module_ns.route("/add")
class AddModule(Resource):
    @module_ns.header("Authorization" "JWT Token", required=True)
    @module_ns.doc(
        summary="Add a module (admin only)",
        description="This endpoint allows you to add a module. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, display_name, description and status. The id will be automatically generated. "
                "The description is optional",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "display_name": {"type": "string"},
                        "description": {"type": "string"},
                        "status": {"type": "int"},
                        "permissions": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"},
                                    "display_name": {"type": "string"},
                                    "description": {"type": "string"},
                                },
                                "required": ["name", "display_name", "permissions"],
                            },
                        },
                    },
                    "required": ["name", "display_name", "status"],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        # Add a module (admin only)
        data = request.get_json()
        return add(data)


@module_ns.route("/edit/<int:module_id>")
class UpdateModule(Resource):
    @module_ns.header("Authorization" "JWT Token", required=True)
    @module_ns.doc(
        summary="Update a module (admin only)",
        description="This endpoint allows you to Update a module. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, display_name, description and status. The id will be automatically generated. "
                "The description is optional",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "display_name": {"type": "string"},
                        "description": {"type": "string"},
                        "status": {"type": "int"},
                        "permissions": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"},
                                    "display_name": {"type": "string"},
                                    "description": {"type": "string"},
                                },
                                "required": ["name", "display_name", "permissions"],
                            },
                        },
                    },
                    "required": ["name", "display_name", "status"],
                },
            },
        },
    )
    @jwt_required()
    def post(self, module_id):
        # Update a module (admin only)
        data = request.get_json()
        return update(data, module_id)


@module_ns.route("/delete/<int:module_id>")
class DeleteModule(Resource):
    @module_ns.header("Authorization" "JWT Token", required=True)
    @module_ns.doc(
        summary="Delete a module (admin only)",
        description="This endpoint allows you to delete a module. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, module_id):
        # Delete a module (admin only)
        return delete(module_id)

@module_ns.route("/datatable")
class DatatableModule(Resource):
    @module_ns.header("Authorization", "JWT Token", required=True)
    @module_ns.doc(
        summary="Get all modules in the database with datatable format (admin only)",
        description="This endpoint returns a list of all modules in the database with datatable format. It is restricted to admin only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data Modules in Datatable Format"""
        return datatable(request.args)
