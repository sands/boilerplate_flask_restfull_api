from flask import jsonify
from .models import Modules, ModuleAddForm, ModuleUpdateForm
from ..permissions.models import Permissions
from ..permissions.controllers import add_by_module
from app import db
from app import db
import json
from datetime import timedelta
import sys

sys.path.append("..")
from api.v1.common.response import api_response
from api.v1.user.controllers import is_admin


def get_module(module_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        module = Modules.query.filter_by(id=module_id).first()
        if module:
            return api_response(
                "success", "Add modules successfully", module.to_dict(), status_code
            )
        else:
            status_code = 400
            return api_response("error", "Module Not Found", response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def get_all_module(data):
    try:
        # return "test"
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        default_page = 1
        default_per_page = 10

        page = int(data.get("page", default_page))
        per_page = int(data.get("per_page", default_per_page))

        # Calculate the offset based on the page number and items per page
        offset = (page - 1) * per_page

        modules = Modules.query
        search_name = data.get("search_name").strip() if data.get("search_name") else ""
        
        # Apply filters for search parameters if provided
        if search_name:
            modules = modules.filter(Modules.name.like(f"%{search_name}%"))

        modules = modules.offset(offset).limit(per_page).all()

        datas = [e.to_dict() for e in modules]
        return api_response(
            "success", "Modules retrieved successfully", datas, status_code
        )

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def add(data):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit user", response_code=status_code
            )

        form = ModuleAddForm(data=data)
        if form.validate():
            status_code = 200

            name = data.get("name")
            display_name = data.get("display_name")
            description = data.get("description")
            status = data.get("status")
            permissions = data.get("permissions")

            module_check = Modules.query.filter_by(name=name).first()
            if module_check:
                status_code = 400
                return api_response(
                    "error", "Name Already Exist", response_code=status_code
                )
            modules = Modules(
                name=name,
                display_name=display_name,
                description=description,
                status=status,
            )
            db.session.add(modules)
            db.session.flush()

            # Add Permissions
            permission_check = json.loads(add_by_module(permissions, modules.id))
            if permission_check["status"] == 200:
                db.session.commit()
                return api_response(
                    "success",
                    "Add modules successfully",
                    modules.to_dict(),
                    status_code,
                )
            else:
                status_code = 400
                return api_response(
                    "error", permission_check["messages"], response_code=status_code
                )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)
    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def update(data, module_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit module", response_code=status_code
            )

        form = ModuleUpdateForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            display_name = data.get("display_name")
            description = data.get("description")
            status = data.get("status")
            permissions = data.get("permissions")

            module_check = Modules.query.filter(
                Modules.name == name, Modules.id != module_id
            ).first()
            module_exist = Modules.query.filter_by(id=module_id).first()
            if module_check:
                status_code = 400
                return api_response(
                    "error", "Name Already Exist", response_code=status_code
                )
            elif not module_exist:
                status_code = 400
                return api_response(
                    "error", "Module Not Found", response_code=status_code
                )
            else:
                module_exist.name = name
                module_exist.display_name = display_name
                module_exist.description = description
                module_exist.status = status

                # Get Permissions Data
                prev_permissions = Permissions.query.filter(
                    Permissions.module_id==module_exist.id
                )
                prev_permissions.delete()
                # Add new permissions
                permission_check = json.loads(
                    add_by_module(permissions, module_exist.id)
                )

                if permission_check["status"] == 200:
                    db.session.commit()
                    return api_response(
                        "success",
                        "Update modules successfully",
                        module_exist.to_dict(),
                        status_code,
                    )
                else:
                    status_code = 400
                    db.session.rollback()
                    return api_response(
                        "error", permission_check["messages"], response_code=status_code
                    )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)
    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def delete(module_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete module", response_code=status_code
            )

        module = Modules.query.filter_by(id=module_id).first()

        if module:
            status_code = 200
            # Delete Module
            db.session.delete(module)

            db.session.commit()
            return api_response(
                "success", "Delete module successfully", module.to_dict(), status_code
            )
        else:
            return api_response("error", "Module Not Found", response_code=400)
    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
    
def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        modules = Modules.query

        if search_value:
            modules = modules.filter(Modules.name.like(f"%{search_value}%"))

        modules = modules.limit(length).offset(start_idx).all()

        # Count the total number of modules
        total_modules = Modules.query.count()

        datas = [e.to_dict() for e in modules]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_modules,
            "recordsFiltered": len(modules),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
