from app import db
from sqlalchemy import func
from wtforms import Form, StringField, validators
from wtforms.validators import *
import json


ModulesStatusDict = {"active": "1", "not_active": "0"}


class Modules(db.Model):
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    display_name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=True)
    status = db.Column(
        db.Enum(ModulesStatusDict["not_active"], ModulesStatusDict["active"]),
        default=ModulesStatusDict["active"],
    )
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=False
    )

    # Define the relationship
    permissions = db.relationship(
        "Permissions", cascade="all, delete-orphan", backref="module", lazy=True
    )

    def __repr__(self):
        return f"Modules(name={self.name}, display_name={self.display_name}, description={self.description}, status={self.status})"

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "display_name": self.display_name,
            "description": self.description,
            "status": self.status,
            "permissions": [permission.to_dict() for permission in self.permissions]
        }


def validate_permissions(form, field):
    try:
        permissions_data = field.data
        if not isinstance(permissions_data, list):
            raise validators.ValidationError("Permissions must be a list of objects.")
        for permission in permissions_data:
            if not isinstance(permission, dict):
                raise validators.ValidationError(
                    "Each permission must be a JSON object."
                )
            if "name" not in permission:
                raise validators.ValidationError(
                    'Each permission must have a "name" field.'
                )
            if "display_name" not in permission:
                raise validators.ValidationError(
                    'Each permission must have a "display_name" field.'
                )
            if "description" not in permission:
                raise validators.ValidationError(
                    'Each permission must have a "description" field.'
                )
    except json.JSONDecodeError:
        raise validators.ValidationError("Invalid JSON format for permissions.")


class ModuleAddForm(Form):
    name = StringField("name", validators=[DataRequired()])
    display_name = StringField("Display Name", validators=[DataRequired()])
    description = StringField("Description", validators=[Optional()])
    status = StringField("Status", validators=[DataRequired()])
    permissions = StringField(
        "permissions", validators=[DataRequired(), validate_permissions]
    )


class ModuleUpdateForm(Form):
    name = StringField("name", validators=[DataRequired()])
    display_name = StringField("Display Name", validators=[DataRequired()])
    description = StringField("Description", validators=[Optional()])
    status = StringField("Status", validators=[DataRequired()])
    permissions = StringField(
        "permissions", validators=[DataRequired(), validate_permissions]
    )
