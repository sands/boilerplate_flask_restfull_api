from app import db
from sqlalchemy import func

class Role_user(db.Model):
    # Define Columns
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True,nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), primary_key=True, nullable=False)

    # Relations
    user = db.relationship('User', foreign_keys=[user_id], backref=db.backref('role_users', lazy=True))
    role = db.relationship('Roles', foreign_keys=[role_id], backref=db.backref('user_roles', lazy=True))