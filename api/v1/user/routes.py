from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import create_access_token, jwt_required
from flask import request

user_ns = Namespace("user", description="Users")


@user_ns.route("/login")
class Login(Resource):
    @user_ns.doc(
        summary="Login",
        description="This endpoint returns a JWT token to be used for authentication. Login with admin credentials to "
        "get admin access. Login with user credentials to get user access. For testing purposes, "
        "the admin id and password are 0 and `admin` respectively. You may also use the user id and "
        "password from the already existing users in the database. Users that already exist in the "
        "database have ids 1 to 409. The password for all of them is `password`. The JWT token is valid "
        "for 1 hour.",
        params={
            "body": {
                "in": "body",
                "description": "Input your id and password",
                "schema": {
                    "type": "object",
                    "properties": {
                        "email": {"type": "string"},
                        "password": {"type": "string"},
                    },
                    "required": ["email", "password"],
                },
            }
        },
    )
    def post(self):
        """Login to the system and get a JWT token (valid for 1 hour) to access the other endpoints"""
        data = request.get_json()
        return signin(data)


@user_ns.route("/all")
class Users(Resource):
    @user_ns.header("Authorization", "JWT Token", required=True)
    @user_ns.doc(
        summary="Get all users in the database (admin only)",
        description="This endpoint returns a list of all users in the database. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all users in the database (admin only)"""
        return get_users(request.args)


@user_ns.route("/get/<int:user_id>")
class UserById(Resource):
    @user_ns.header("Authorization" "JWT Token", required=True)
    @user_ns.doc(
        summary="Get user in the database",
        description="This endpoint return user data in the database.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, user_id):
        """Get a user by id (admin only)"""
        return get_user(user_id)


@user_ns.route("/register")
class Register(Resource):
    @user_ns.doc(
        summary="Register",
        description="This endpoint allows you to register a new user.",
        params={
            "body": {
                "in": "body",
                "description": "Input your fullname, division_id, role_id, department_id, role_job_id, position_id, email, username and password. The id will be automatically generated. "
                'The id will be used to login. The password is optional and will be set to "123456789" if not '
                "provided.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "fullname": {"type": "string"},
                        "division_id": {"type": "int"},
                        "role_id": {"type": "int"},
                        "department_id": {"type": "int"},
                        "role_job_id": {"type": "int"},
                        "position_id": {"type": "int"},
                        "email": {"type": "string"},
                        "username": {"type": "string"},
                        "password": {"type": "string"},
                    },
                    "required": [
                        "fullname",
                        "username",
                        "email",
                        "role_id",
                        "position_id",
                        "division_id",
                        "department_id",
                        "role_job_id",
                    ],
                },
            }
        },
    )
    def post(self):
        """Register a new user"""
        data = request.get_json()
        return signup(data)


@user_ns.route("/add")
class AddUser(Resource):
    @user_ns.header("Authorization" "JWT Token", required=True)
    @user_ns.doc(
        summary="Add a user (admin only)",
        description="This endpoint allows you to add a user. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input your fullname, division_id, role_id, department_id, role_job_id, position_id, email, username and password. The id will be automatically generated. "
                'The id will be used to login. The password is optional and will be set to "123456789" if not '
                "provided.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "fullname": {"type": "string"},
                        "jenis_kelamin": {"type": "string"},
                        "ttl": {"type": "string"},
                        "address": {"type": "string"},
                        "division_id": {"type": "int"},
                        "nik": {"type": "int"},
                        "role_id": {"type": "int"},
                        "unit_id": {"type": "int"},
                        "department_id": {"type": "int"},
                        "role_job_id": {"type": "int"},
                        "position_id": {"type": "int"},
                        "email": {"type": "string"},
                        "username": {"type": "string"},
                        "password": {"type": "string"},
                        "status": {"type": "int"},
                    },
                    "required": [
                        "fullname",
                        "username",
                        "email",
                        "role_id",
                        "position_id",
                        "division_id",
                        "department_id",
                        "role_job_id",
                        "status",
                        "nik",
                    ],
                },
            },
        },
    )
    def post(self):
        """Add a new user (admin only)"""
        data = request.get_json()
        return add_user(data)


@user_ns.route("/edit/<int:user_id>")
class EditUser(Resource):
    @user_ns.header("Authorization", "JWT Token", required=True)
    @user_ns.doc(
        summary="Update a user (admin only)",
        description="This endpoint allows you to update a user. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input your fullname, division_id, role_id, department_id, role_job_id, position_id, email, username and password. "
                "Only the fields you want to update are required.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "fullname": {"type": "string"},
                        "jenis_kelamin": {"type": "string"},
                        "ttl": {"type": "string"},
                        "address": {"type": "string"},
                        "division_id": {"type": "int"},
                        "nik": {"type": "int"},
                        "role_id": {"type": "int"},
                        "unit_id": {"type": "int"},
                        "department_id": {"type": "int"},
                        "role_job_id": {"type": "int"},
                        "position_id": {"type": "int"},
                        "email": {"type": "string"},
                        "username": {"type": "string"},
                        "password": {"type": "string"},
                        "status": {"type": "int"},
                    },
                },
            },
        },
    )
    @jwt_required()
    def put(self, user_id):
        """Edit a user (admin only)"""
        data = request.get_json()
        return update(data, user_id)


@user_ns.route("/delete/<int:user_id>")
class DeleteUser(Resource):
    @user_ns.header("Authorization", "JWT Token", required=True)
    @user_ns.doc(
        summary="Delete an user",
        description="This endpoint allows you to delete an user. It is restricted to admins",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, user_id):
        """Delete user by id for admin"""
        return delete(user_id)


@user_ns.route("/datatable")
class DatatableUser(Resource):
    @user_ns.header("Authorization", "JWT Token", required=True)
    @user_ns.doc(
        summary="Get all users in the database with datatable format (admin only)",
        description="This endpoint returns a list of all users in the database with datatable format. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data User in Datatable Format"""
        return datatable(request.args)
