from flask import request, jsonify
from ..roles.models import *
from ..role_user.models import Role_user
from .models import (
    User,
    UserLoginForm,
    UserRegisterForm,
    UserUpdateForm,
    UserAddForm,
    UserStatusDict,
)
from flask_jwt_extended import create_access_token
from app import app, db
import jwt
import datetime
from datetime import timedelta
from werkzeug.security import generate_password_hash, check_password_hash
import sys

sys.path.append("..")
from api.v1.common.response import api_response


def get_user_id_from_token():
    token = request.headers.get("Authorization")

    if token:
        token = token.split(" ")[1]
        try:
            payload = jwt.decode(
                token, app.config["JWT_SECRET_KEY"], algorithms=["HS256"]
            )
            user_id = payload["sub"]["user_id"]
            return user_id
        except:
            return
    return


def get_username_from_token():
    token = request.headers.get("Authorization")
    if token:
        token = token.split(" ")[1]
        try:
            payload = jwt.decode(
                token, app.config["JWT_SECRET_KEY"], algorithms=["HS256"]
            )
            username = payload["sub"]["username"]
            return username
        except:
            return
    return


def is_admin():
    if get_username_from_token() == "admin":
        return True
    return False


def get_user(user_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        user = User.query.filter_by(id=user_id).first()
        if user:
            user = user.to_dict()
            return api_response(
                "success", "User retrieved successfully", user, status_code
            )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def get_users(data):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        default_page = 1
        default_per_page = 10

        page = int(data.get("page", default_page))
        per_page = int(data.get("per_page", default_per_page))

        # Calculate the offset based on the page number and items per page
        offset = (page - 1) * per_page

        users = User.query
        search_name = data.get("search_name").strip() if data.get("search_name") else ""
        search_email = (
            data.get("search_email") if data.get("search_email") else ""
        )

        # Apply filters for search parameters if provided
        if search_name:
            users = users.filter(User.fullname.like(f"%{search_name}%"))
        if search_email:
            users = users.filter(User.email.like(f"%{search_email}%"))

        users = users.offset(offset).limit(per_page).all()

        datas = [e.to_dict() for e in users]
        return api_response(
            "success", "Users retrieved successfully", datas, status_code
        )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def signin(data):
    try:
        form = UserLoginForm(data=data)

        if form.validate():
            status_code = 200
            email = data.get("email")
            password = data.get("password")

            user = User.query.filter_by(email=email).first()

            if user:
                # Verify the provided password against the hashed password
                if check_password_hash(user.password_hash, password):
                    # Passwords match, login successful
                    payload = {
                        "user_id": user.id,
                        "username": user.username,
                        "exp": datetime.datetime.utcnow() + datetime.timedelta(days=1),
                    }
                    access_token = create_access_token(
                        identity=payload, expires_delta=timedelta(days=1), fresh=True
                    )
                    # access_token_str = access_token.decode('utf-8')
                    return {"access_token": access_token}, status_code

            status_code = 422
            return api_response("error", "Data not found", response_code=status_code)
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def signup(data):
    try:
        form = UserRegisterForm(data=data)

        if form.validate():
            status_code = 200
            fullname = data.get("fullname")
            division_id = data.get("division_id")
            department_id = data.get("department_id")
            role_job_id = data.get("role_job_id")
            role_id = (
                Roles.get_role_user_id()
                if data.get("role_id") == Roles.get_role_superadmin_id()
                else data.get("role_id")
            )  # superadmin = 1, auto change to user if role id is 1
            position_id = data.get("position_id")
            department_id = data.get("department_id")
            fullname = data.get("fullname")
            username = data.get("username")
            email = data.get("email")
            password = data.get("password")
            status = UserStatusDict["active"]
            user_check = User.query.filter_by(email=email).first()

            if user_check:
                status_code = 400
                return api_response("error", "Email Taken", response_code=status_code)
            last_user_item = User.query.order_by(User.id.desc()).first()
            new_user_id = last_user_item.id + 1
            user = User(
                id=new_user_id,
                fullname=fullname,
                username=username,
                email=email,
                password_hash=generate_password_hash(password),
                division_id=division_id,
                department_id=department_id,
                role_job_id=role_job_id,
                position_id=position_id,
                created_by=new_user_id,
                updated_by=new_user_id,
                status=status,
            )
            db.session.add(user)

            # After save to user and then save to role_user
            role = Roles.query.filter_by(id=role_id).first()

            if not role:
                status_code = 400
                return api_response(
                    "error", "Role Data Not Found", response_code=status_code
                )

            user_role = Role_user(user_id=user.id, role_id=role_id)
            db.session.add(user_role)

            db.session.commit()

            return api_response(
                "success", "Register user successfully", user.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def add_user(data):
    try:
        form = UserAddForm(data=data)

        if form.validate():
            status_code = 200

            fullname = data.get("fullname")
            address = data.get("address")
            jenis_kelamin = data.get("jenis_kelamin")
            ttl = data.get("ttl")
            unit_id = data.get("unit_id")
            nik = data.get("nik")
            division_id = data.get("division_id")
            department_id = data.get("department_id")
            role_job_id = data.get("role_job_id")
            role_id = (
                Roles.get_role_user_id()
                if data.get("role_id") == Roles.get_role_superadmin_id()
                else data.get("role_id")
            )  # superadmin = 1, auto change to user if role id is 1
            position_id = data.get("position_id")
            department_id = data.get("department_id")
            fullname = data.get("fullname")
            username = data.get("username")
            email = data.get("email")
            password = data.get("password")
            status = UserStatusDict["active"]

            user_check = User.query.filter_by(email=email).first()

            if user_check:
                status_code = 400
                return api_response("error", "Email Taken", response_code=status_code)
            last_user_item = User.query.order_by(User.id.desc()).first()
            new_user_id = last_user_item.id + 1
            user = User(
                id=new_user_id,
                address=address,
                jenis_kelamin=jenis_kelamin,
                ttl=ttl,
                unit_id=unit_id,
                nik=nik,
                fullname=fullname,
                username=username,
                email=email,
                password_hash=generate_password_hash(password),
                division_id=division_id,
                department_id=department_id,
                role_job_id=role_job_id,
                position_id=position_id,
                created_by=new_user_id,
                updated_by=new_user_id,
                status=status,
            )
            db.session.add(user)

            # After save to user and then save to role_user
            role = Roles.query.filter_by(id=role_id).first()

            if not role:
                status_code = 400
                return api_response(
                    "error", "Role Data Not Found", response_code=status_code
                )

            user_role = Role_user(user_id=user.id, role_id=role_id)
            db.session.add(user_role)

            db.session.commit()

            return api_response(
                "success", "Add user successfully", user.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def update(data, user_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit user", response_code=status_code
            )

        form = UserUpdateForm(data=data)

        if form.validate():
            status_code = 200

            fullname = data.get("fullname")
            address = data.get("address")
            jenis_kelamin = data.get("jenis_kelamin")
            ttl = data.get("ttl")
            unit_id = data.get("unit_id")
            nik = data.get("nik")
            division_id = data.get("division_id")
            department_id = data.get("department_id")
            role_job_id = data.get("role_job_id")
            role_id = (
                Roles.get_role_user_id()
                if data.get("role_id") == Roles.get_role_superadmin_id()
                else data.get("role_id")
            )  # superadmin = 1, auto change to user if role id is 1
            position_id = data.get("position_id")
            department_id = data.get("department_id")
            fullname = data.get("fullname")
            username = data.get("username")
            email = data.get("email")
            password = data.get("password")
            status = UserStatusDict["active"]

            user = User.query.filter_by(id=user_id).first()

            if user:
                if "fullname" in data:
                    if User.query.filter_by(fullname=fullname).first():
                        return api_response("error", "name taken", response_code=400)
                    user.fullname = fullname
                if "address" in data:
                    user.address = address
                if "jenis_kelamin" in data:
                    user.jenis_kelamin = jenis_kelamin
                if "ttl" in data:
                    user.ttl = ttl
                if "unit_id" in data:
                    user.unit_id = unit_id
                if "nik" in data:
                    user.nik = nik
                if "division_id" in data:
                    user.division_id = division_id
                if "department_id" in data:
                    user.department_id = department_id
                if "role_job_id" in data:
                    user.role_job_id = role_job_id
                if "role_id" in data:
                    user.role_id = role_id
                if "position_id" in data:
                    user.position_id = position_id
                if "username" in data:
                    if (
                        User.query.filter_by(username=username)
                        .filter(username != user.username)
                        .first()
                    ):
                        return api_response(
                            "error", "username taken", response_code=400
                        )
                    user.username = username
                if "email" in data:
                    if (
                        User.query.filter_by(email=email)
                        .filter(email != user.email)
                        .first()
                    ):
                        return api_response("error", "email taken", response_code=400)
                    user.email = email
                if "password" in data:
                    user.password_hash = generate_password_hash(password)
                if "status" in data:
                    user.status = status
            else:
                return api_response("error", "User Not Found", response_code=400)

            db.session.commit()

            return api_response(
                "success", "Update user successfully", user.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def delete(user_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete user", response_code=status_code
            )

        user = User.query.filter_by(id=user_id).first()

        if user:
            status_code = 200
            # Delete Role User
            role_user = Role_user.query.filter_by(user_id=user.id).first()
            db.session.delete(role_user)

            # Delete User
            db.session.delete(user)

            db.session.commit()
            return api_response(
                "success", "Delete user successfully", user.to_dict(), status_code
            )
        else:
            return api_response("error", "User Not Found", response_code=400)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        users = User.query

        if search_value:
            users = users.filter(User.fullname.like(f"%{search_value}%"))

        users = users.limit(length).offset(start_idx).all()

        # Count the total number of users
        total_users = User.query.count()

        datas = [e.to_dict() for e in users]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_users,
            "recordsFiltered": len(users),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
