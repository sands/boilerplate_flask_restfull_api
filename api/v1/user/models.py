from app import db
from sqlalchemy import func
from werkzeug.security import generate_password_hash, check_password_hash
from wtforms import Form, StringField
from wtforms.validators import DataRequired, Email
from enum import Enum

UserJenisKelaminDict = {"laki": "M", "perempuan": "F"}
UserStatusDict = {"active": "1", "not_active": "0"}


class User(db.Model):
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, nullable=False, autoincrement=True)
    division_id = db.Column(db.Integer, db.ForeignKey("m_division.id"), nullable=True)
    department_id = db.Column(
        db.Integer, db.ForeignKey("m_department.id"), nullable=True
    )
    unit_id = db.Column(db.Integer, db.ForeignKey("m_unit.id"), nullable=True)
    role_job_id = db.Column(db.Integer, db.ForeignKey("m_role_job.id"), nullable=True)
    position_id = db.Column(db.Integer, db.ForeignKey("m_position.id"), nullable=True)
    fullname = db.Column(db.String(255), nullable=True)
    username = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(255), nullable=False)
    email_verified_at = db.Column(db.DateTime, nullable=True)
    password_hash = db.Column(db.String(128), nullable=False)
    status = db.Column(
        db.Enum(UserStatusDict["not_active"], UserStatusDict["active"]),
        default=UserStatusDict["active"],
    )
    nik = db.Column(db.Integer, nullable=True)
    ttl = db.Column(db.String(255), nullable=True)
    jenis_kelamin = db.Column(
        db.Enum(UserJenisKelaminDict["laki"], UserJenisKelaminDict["perempuan"]),
        nullable=True,
    )
    address = db.Column(db.Text, nullable=True)
    remember_token = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=False
    )
    created_by = db.Column(db.Integer, nullable=False)
    updated_by = db.Column(db.Integer, nullable=False)

    # Relations
    # division = db.relationship('M_division', foreign_keys=[division_id], backref=db.backref('users', lazy=True))
    # department = db.relationship('M_department', foreign_keys=[department_id], backref=db.backref('users', lazy=True))
    # unit = db.relationship('M_unit', foreign_keys=[unit_id], backref=db.backref('users', lazy=True))
    # role_job = db.relationship('M_role_job', foreign_keys=[role_job_id], backref=db.backref('users', lazy=True))
    # position = db.relationship('M_position', foreign_keys=[position_id], backref=db.backref('users', lazy=True))
    # created_by_user = db.relationship('User', foreign_keys=[created_by], backref=db.backref('created_records', lazy=True))
    # updated_by_user = db.relationship('User', foreign_keys=[updated_by], backref=db.backref('updated_records', lazy=True))

    def __repr__(self):
        return f"User(fullname={self.fullname}, email={self.email}, nik={self.nik}, status={self.status})"

    def to_dict(self):
        return {
            "id": self.id,
            "fullname": self.fullname,
            "email": self.email,
            "nik": self.nik,
            "status": self.status,
        }

    @property
    def password(self):
        raise AttributeError("Password is not a readable attribute.")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def serialize(self):
        return {"id": self.id, "name": self.name, "email": self.email}


class UserLoginForm(Form):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = StringField("Password", validators=[DataRequired()])


class UserRegisterForm(Form):
    email = StringField("Email", validators=[DataRequired(), Email()])
    fullname = StringField("Fullname", validators=[DataRequired()])
    division_id = StringField("Division Id", validators=[DataRequired()])
    department_id = StringField("Department Id", validators=[DataRequired()])
    role_job_id = StringField("Role Job Id", validators=[DataRequired()])
    role_id = StringField("Role Id", validators=[DataRequired()])
    position_id = StringField("Position Id", validators=[DataRequired()])
    username = StringField("Username", validators=[DataRequired()])
    password = StringField("Password", validators=[DataRequired()])


class UserAddForm(Form):
    email = StringField("Email", validators=[DataRequired(), Email()])
    fullname = StringField("Fullname", validators=[DataRequired()])
    division_id = StringField("Division Id", validators=[DataRequired()])
    department_id = StringField("Department Id", validators=[DataRequired()])
    role_job_id = StringField("Role Job Id", validators=[DataRequired()])
    unit_id = StringField("Unit Id", validators=[DataRequired()])
    role_id = StringField("Role Id", validators=[DataRequired()])
    position_id = StringField("Position Id", validators=[DataRequired()])
    nik = StringField("NIK", validators=[DataRequired()])
    jenis_kelamin = StringField("Jenis Kelamin", validators=[DataRequired()])
    ttl = StringField("TTL", validators=[DataRequired()])
    address = StringField("Address", validators=[DataRequired()])
    username = StringField("Username", validators=[DataRequired()])
    password = StringField("Password", validators=[DataRequired()])
    status = StringField("Status", validators=[DataRequired()])


class UserUpdateForm(Form):
    email = StringField("Email", validators=[Email()])
