from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import jwt_required
from flask import request

permission_role_ns = Namespace("permission-role", description="Permission Role")


@permission_role_ns.route("/attach")
class attachPermission(Resource):
    @permission_role_ns.header("Authorization" "JWT Token", required=True)
    @permission_role_ns.doc(
        summary="Attach Permission (admin only)",
        description="This endpoint allows you to attach permission to Module. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input role_id and permission_id.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "role_id": {"type": "integer"},
                        "permission_id": {"type": "integer"},
                    },
                    "required": ["role_id", "permission_id"],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        """Attach Permission to Module (admin only)"""
        data = request.get_json()
        return attachPerm(data)


@permission_role_ns.route("/detach")
class detachPermission(Resource):
    @permission_role_ns.header("Authorization" "JWT Token", required=True)
    @permission_role_ns.doc(
        summary="Detach Permission (admin only)",
        description="This endpoint allows you to Detach permission to Module. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input role_id and permission_id.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "role_id": {"type": "integer"},
                        "permission_id": {"type": "integer"},
                    },
                    "required": ["role_id", "permission_id"],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        """Detach Permission to Module (admin only)"""
        data = request.get_json()
        return detachPerm(data)
