from flask import jsonify
from .models import *
from ..user.controllers import is_admin, get_user_id_from_token
from app import db
import sys

sys.path.append("..")
from api.v1.common.response import api_response


def attachPerm(data):
    try:
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        form = AttachPermForm(data=data)

        if form.validate():
            status_code = 200

            role_id = data.get("role_id")
            permission_id = data.get("permission_id")
            data_check = Permissions_role.query.filter_by(
                role_id=role_id, permission_id=permission_id
            ).first()

            if data_check:
                status_code = 400
                return api_response(
                    "error",
                    "Attach Permission Role Unsuccessfully",
                    response_code=status_code,
                )

            new_perm = Permissions_role(permission_id=permission_id, role_id=role_id)
            db.session.add(new_perm)
            db.session.commit()

            return api_response(
                "success", "Attach Permission Role Successfully", None, status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def detachPerm(data):
    try:
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        form = DetachPermForm(data=data)

        if form.validate():
            status_code = 200

            role_id = data.get("role_id")
            permission_id = data.get("permission_id")
            data_check = Permissions_role.query.filter_by(
                role_id=role_id, permission_id=permission_id
            ).first()

            if data_check:
                db.session.delete(data_check)
                db.session.commit()
            else:
                status_code = 404
                return api_response(
                    "error", "Data Not Found", response_code=status_code
                )

            return api_response(
                "success", "Detach Permission Role Successfully", None, status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
