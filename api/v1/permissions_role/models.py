from app import db
from sqlalchemy import func
from wtforms import Form, StringField, IntegerField, validators
from wtforms.validators import *


class Permissions_role(db.Model):
    # Define Columns
    permission_id = db.Column(
        db.Integer, db.ForeignKey("permissions.id"), primary_key=True, nullable=False
    )
    role_id = db.Column(
        db.Integer, db.ForeignKey("roles.id"), primary_key=True, nullable=False
    )

    # Relations
    # permissions = db.relationship('Permissions', foreign_keys=[permission_id], backref=db.backref('role_permission', lazy=True))
    # role = db.relationship('Roles', foreign_keys=[role_id], backref=db.backref('permission_role', lazy=True))


class AttachPermForm(Form):
    role_id = IntegerField("Role Id", validators=[DataRequired()])
    permission_id = IntegerField("Permission Id", validators=[DataRequired()])

class DetachPermForm(Form):
    role_id = IntegerField("Role Id", validators=[DataRequired()])
    permission_id = IntegerField("Permission Id", validators=[DataRequired()])
