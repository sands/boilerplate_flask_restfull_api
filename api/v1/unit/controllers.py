from flask import jsonify
from .models import *
from ..user.controllers import is_admin, get_user_id_from_token
from app import db
import sys

sys.path.append("..")
from api.v1.common.response import api_response


def get_unit(unit_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        unit = M_unit.query.filter_by(id=unit_id).first()
        if unit:
            unit = unit.to_dict()
            return api_response(
                "success", "Unit retrieved successfully", unit, status_code
            )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def get_units(data):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        default_page = 1
        default_per_page = 10

        page = int(data.get("page", default_page))
        per_page = int(data.get("per_page", default_per_page))

        # Calculate the offset based on the page number and items per page
        offset = (page - 1) * per_page

        units = M_unit.query
        search_code = data.get("search_code").strip() if data.get("search_code") else ""
        search_name = data.get("search_name") if data.get("search_name") else ""

        # Apply filters for search parameters if provided
        if search_name:
            units = units.filter(M_unit.name.like(f"%{search_name}%"))
        if search_code:
            units = units.filter(M_unit.code.like(f"%{search_code}%"))

        units = units.offset(offset).limit(per_page).all()

        datas = [e.to_dict() for e in units]
        return api_response(
            "success", "Unit retrieved successfully", datas, status_code
        )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def add_unit(data):
    try:
        form = UnitAddForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            code = data.get("code")
            department_id = data.get("department_id")
            status = data.get("status")
            description = data.get("description")
            unt_check = M_unit.query.filter_by(code=code).first()

            if unt_check:
                status_code = 400
                return api_response("error", "Unit Taken", response_code=status_code)
            
            last_unt_item = M_unit.query.order_by(M_unit.id.desc()).first() 
            new_unt_id = last_unt_item.id + 1 if last_unt_item is not None else 1

            new_unit = M_unit(
                id=new_unt_id,
                name=name,
                department_id=department_id,
                code=code,
                status=status,
                description=description,
                created_by=get_user_id_from_token(),
            )
            db.session.add(new_unit)
            db.session.commit()

            return api_response(
                "success", "Add Unit successfully", new_unit.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def update(data, unit_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit rolejob", response_code=status_code
            )

        form = UnitUpdateForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            department_id = data.get("department_id")
            code = data.get("code")
            status = data.get("status")
            description = data.get("description")

            unit = M_unit.query.filter_by(id=unit_id).first()
            unit.updated_by = get_user_id_from_token()

            if unit:
                if "name" in data:
                    if M_unit.query.filter_by(name=name).first():
                        return api_response("error", "Unit taken", response_code=400)
                    unit.name = name
                if "department_id" in data:
                    unit.department_id = department_id
                if "code" in data:
                    if M_unit.query.filter(M_unit.id != unit_id, M_unit.code == code).first():
                        return api_response("error","Code taken",response_code=400)
                    unit.code = code
                if "description" in data:
                    unit.description = description
                if "status" in data:
                    unit.status = status
            else:
                return api_response("error", "Unit Not Found", response_code=400)

            db.session.commit()

            return api_response(
                "success", "Update Unit successfully", unit.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def delete(unit_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete Unit", response_code=status_code
            )

        unit = M_unit.query.filter_by(id=unit_id).first()

        if unit:
            status_code = 200

            # Delete Unit
            db.session.delete(unit)
            db.session.commit()

            return api_response(
                "success", "Delete Unit successfully", None, status_code
            )
        else:
            return api_response("error", "Unit Not Found", response_code=400)
    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        unit = M_unit.query

        if search_value:
            unit = unit.filter(M_unit.name.like(f"%{search_value}%"))

        unit = unit.limit(length).offset(start_idx).all()

        # Count the total number of units
        total_unit = M_unit.query.count()

        datas = [e.to_dict() for e in unit]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_unit,
            "recordsFiltered": len(unit),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
