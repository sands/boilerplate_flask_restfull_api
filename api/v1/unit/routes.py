from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import jwt_required
from flask import request

unit_ns = Namespace("unit", description="Master Unit")


@unit_ns.route("/all")
class Unit(Resource):
    @unit_ns.header("Authorization", "JWT Token", required=True)
    @unit_ns.doc(
        summary="Get all Unit in the database (admin only)",
        description="This endpoint returns a list of all Unit in the database. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all Unit in the database (admin only)"""
        return get_units(request.args)


@unit_ns.route("/get/<int:unit_id>")
class UnitById(Resource):
    @unit_ns.header("Authorization" "JWT Token", required=True)
    @unit_ns.doc(
        summary="Get Unit in the database",
        description="This endpoint return Unit data in the database.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, unit_id):
        """Get a Unit by id (admin only)"""
        return get_unit(unit_id)


@unit_ns.route("/add")
class AddUnit(Resource):
    @unit_ns.header("Authorization" "JWT Token", required=True)
    @unit_ns.doc(
        summary="Add a Unit (admin only)",
        description="This endpoint allows you to add a Unit. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, department_id, code, status('0' = Active and '1' = Not Active) and description. The id will be automatically generated. ",
                "schema": {
                    "type": "object",
                    "properties": {
                        "code": {"type": "string"},
                        "name": {"type": "string"},
                        "department_id": {"type": "integer"},
                        "status": {"type": "string"},
                        "description": {"type": "string"},
                    },
                    "required": [
                        "code",
                        "department_id",
                        "name",
                        "status",
                    ],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        """Add a new Unit (admin only)"""
        data = request.get_json()
        return add_unit(data)


@unit_ns.route("/edit/<int:unit_id>")
class EditUnit(Resource):
    @unit_ns.header("Authorization", "JWT Token", required=True)
    @unit_ns.doc(
        summary="Update a Unit (admin only)",
        description="This endpoint allows you to update a Unit. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, department_id, code, status('0' = Active and '1' = Not Active) and description."
                "Only the fields you want to update are required.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "code": {"type": "string"},
                        "department_id": {"type": "integer"},
                        "status": {"type": "string"},
                        "description": {"type": "string"},
                    },
                },
            },
        },
    )
    @jwt_required()
    def put(self, unit_id):
        """Edit a Unit (admin only)"""
        data = request.get_json()
        return update(data, unit_id)


@unit_ns.route("/delete/<int:unit_id>")
class DeleteUnit(Resource):
    @unit_ns.header("Authorization", "JWT Token", required=True)
    @unit_ns.doc(
        summary="Delete an Unit",
        description="This endpoint allows you to delete an Unit. It is restricted to admins",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, unit_id):
        """Delete Unit by id for admin"""
        return delete(unit_id)


@unit_ns.route("/datatable")
class DatatableUnit(Resource):
    @unit_ns.header("Authorization", "JWT Token", required=True)
    @unit_ns.doc(
        summary="Get all Units in the database with datatable format (admin only)",
        description="This endpoint returns a list of all Units in the database with datatable format. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data Unit in Datatable Format"""
        return datatable(request.args)
