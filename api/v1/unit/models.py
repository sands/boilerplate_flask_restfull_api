from app import db
from sqlalchemy import func
from wtforms import Form, StringField, IntegerField
from wtforms.validators import DataRequired
from enum import Enum

UserStatusDict = {"active": "1", "not_active": "0"}


class M_unit(db.Model):
    __tablename__ = "m_unit"
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.String(255), nullable=False)
    department_id = db.Column(
        db.Integer, db.ForeignKey("m_department.id"), nullable=False
    )
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=True)
    status = db.Column(
        db.Enum(UserStatusDict["not_active"], UserStatusDict["active"]),
        default=UserStatusDict["active"],
    )
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=True
    )
    created_by = db.Column(db.Integer, nullable=False)
    updated_by = db.Column(db.Integer, nullable=True)

    # Relations
    # department = db.relationship('M_department', foreign_keys=[department_id], backref=db.backref('unit', lazy=True))
    # created_by_user = db.relationship('User', foreign_keys=[created_by], backref=db.backref('created_units', lazy=True))
    # updated_by_user = db.relationship('User', foreign_keys=[updated_by], backref=db.backref('updated_units', lazy=True))

    def __repr__(self):
        return f"M_unit(code={self.code}, name={self.name}, department_name={self.department.name}, department_id={self.department_id}, description={self.description}, status={self.status})"

    def to_dict(self):
        return {
            "id": self.id,
            "code": self.code,
            "name": self.name,
            "division_name": self.department.division.name,
            "department_name": self.department.name,
            "description": self.description,
            "status": self.status,
        }


class UnitAddForm(Form):
    code = StringField("Code", validators=[DataRequired()])
    department_id = IntegerField("Department Id", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    status = StringField("Status", validators=[DataRequired()])


class UnitUpdateForm(Form):
    code = StringField("Code", validators=[DataRequired()])
    department_id = IntegerField("Department Id", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    status = StringField("Status", validators=[DataRequired()])
