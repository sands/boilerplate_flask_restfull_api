from flask import jsonify
from .models import *
from ..user.controllers import is_admin, get_user_id_from_token
from app import db
from sqlalchemy import text
import sys
sys.path.append("..")
from api.v1.common.response import api_response


def get_menus(data):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        default_page = 1
        default_per_page = 1000

        page = int(data.get("page", default_page))
        per_page = int(data.get("per_page", default_per_page))

        # Calculate the offset based on the page number and items per page
        offset = (page - 1) * per_page

        menus = Menus.query
        search_name = data.get("search_name") if data.get("search_name") else ""

        # Apply filters for search parameters if provided
        if search_name:
            menus = menus.filter(Menus.name.like(f"%{search_name}%"))

        menus = menus.offset(offset).limit(per_page).all()

        new_menus = {}
        for menu in menus:
            if menu.parent_id != 0:
                data_child = {
                    "name": menu.name,
                    "display_name": menu.display_name,
                    "icon": menu.icon,
                    "url": menu.url,
                    "permission_id": menu.permission_id,
                }

                if "children" in new_menus[menu.parent_id]:
                    new_menus[menu.parent_id]["children"][menu.id] = data_child
                else:
                    new_menus[menu.parent_id]["children"] = {}
                    new_menus[menu.parent_id]["children"][menu.id] = data_child
            else:
                new_menus[menu.id] = {
                    "name": menu.name,
                    "display_name": menu.display_name,
                    "icon": menu.icon,
                    "url": menu.url,
                    "permission_id": menu.permission_id,
                }

        return api_response(
            "success", "Menu retrieved successfully", new_menus, status_code
        )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def add_menu(data):
    try:
        form = MenusAddForm(data=data)

        if form.validate():
            status_code = 200

            menu_json = data["menu_json"]

            stmt = text('TRUNCATE TABLE menus')
            db.session.execute(stmt)

            seq = 1
            for menu in menu_json:
                last_menu_item = Menus.query.order_by(Menus.id.desc()).first()
                new_menu_parent_id = last_menu_item.id + 1 if last_menu_item is not None else 1
                menu_store = Menus(
                    id=new_menu_parent_id,
                    icon=menu['icon'],
                    name=menu['name'],
                    display_name=menu['display_name'],
                    url=menu['url'],
                    permission_id=menu['permission_id'],
                    parent_id=0,
                    seq=seq,
                )
                db.session.add(menu_store)

                if "children" in menu:
                    seq_child = 1
                    for child in menu['children']: 
                        last_menu_item = Menus.query.order_by(Menus.id.desc()).first()
                        new_menu_child_id = last_menu_item.id + 1 if last_menu_item is not None else 1
                        sub_menu = Menus(
                            id=new_menu_child_id,
                            icon=child['icon'],
                            name=child['name'],
                            display_name=child['display_name'],
                            url=child['url'],
                            seq=seq_child,
                            parent_id=menu_store.id,
                            permission_id=child['permission_id'],
                        )
                        db.session.add(sub_menu)
                        seq_child=seq_child+1

                seq=seq+1
             
                db.session.commit()

            return api_response(
                "success", "Add menu successfully", None, status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
