from app import db
from sqlalchemy import func
from wtforms import Form, StringField, IntegerField
from wtforms.validators import DataRequired, Email
from enum import Enum


class Menus(db.Model):
    __tablename__ = "menus"
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    parent_id = db.Column(db.Integer, nullable=False)
    permission_id = db.Column(
        db.Integer, db.ForeignKey("permissions.id"), nullable=False
    )
    name = db.Column(db.String(255), nullable=False)
    display_name = db.Column(db.String(255), nullable=False)
    url = db.Column(db.String(255), nullable=True)
    description = db.Column(db.Text, nullable=True)
    icon = db.Column(db.String(255), nullable=True)
    icon_child = db.Column(db.String(255), nullable=True)
    seq = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=False
    )

    def __repr__(self):
        return f"Menus(id={self.id},parent_id={self.parent_id}, permission_id={self.permission_id}, name={self.name}, display_name={self.display_name},url={self.url}, description={self.description}, icon={self.icon}, icon_child={self.icon_child}, seq={self.seq})"

    def to_dict(self):
        return {
            "id": self.id,
            "parent_id": self.parent_id,
            "permission_id": self.permission_id,
            "name": self.name,
            "display_name": self.display_name,
            "url": self.url,
            "description": self.description,
            "icon": self.icon,
            "icon_child": self.icon_child,
            "seq": self.seq,
        }


class MenusAddForm(Form):
    menu_json = StringField("Menu JSON", validators=[DataRequired()])

