from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import create_access_token, jwt_required
from flask import request

menu_ns = Namespace("menus", description="Menus")


@menu_ns.route("/all")
class Menu(Resource):
    @menu_ns.header("Authorization", "JWT Token", required=True)
    @menu_ns.doc(
        summary="Get all Menu in the database (admin only)",
        description="This endpoint returns a list of all Menu in the database. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all Menu in the database (admin only)"""
        return get_menus(request.args)


@menu_ns.route("/add")
class MenuById(Resource):
    @menu_ns.header("Authorization" "JWT Token", required=True)
    @menu_ns.doc(
        summary="Get Menu in the database",
        description="This endpoint return Menu data in the database.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, menu_id):
        """Get a Menu by id (admin only)"""
        return get_menu(menu_id)


@menu_ns.route("/add")
class AddMenu(Resource):
    @menu_ns.header("Authorization" "JWT Token", required=True)
    @menu_ns.doc(
        summary="Add a Menu (admin only)",
        description="This endpoint allows you to add a Menu. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input menu_json : Sample "
                "{'icon': 'fa fa-home text-primary','name': 'dashboard','display_name': 'Dashboard','url': 'home','permission_id': 1}",
                "schema": {
                    "type": "object",
                    "properties": {
                        "menu_json": {"type": "string"},
                    },
                    "required": ["menu_json"],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        """Add a new Menu (admin only)"""
        # data = request.get_json()
        return add_menu(request.json)
