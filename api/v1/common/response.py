############  FLASK BOILERPLATE REST API by SANDS #####################
from flask import jsonify, make_response

def api_response(status, message, data=None, response_code=200):
    # Create the response
    response_data = {
        'status': status,
        'message': message
    }
        
    if data is not None:
        response_data['data'] = data

    response = make_response(jsonify(response_data), response_code)

    # Set additional headers if needed
    response.headers["Content-Type"] = "application/json"

    return response
