from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import jwt_required
from flask import request

role_job_ns = Namespace("role-job", description="Master Role Job User")


@role_job_ns.route("/all")
class Rolejob(Resource):
    @role_job_ns.header("Authorization", "JWT Token", required=True)
    @role_job_ns.doc(
        summary="Get all role job in the database (admin only)",
        description="This endpoint returns a list of all role job in the database. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all role job in the database (admin only)"""
        return get_role_jobs()


@role_job_ns.route("/get/<int:role_job_id>")
class RolejobById(Resource):
    @role_job_ns.header("Authorization" "JWT Token", required=True)
    @role_job_ns.doc(
        summary="Get role job in the database",
        description="This endpoint return role job data in the database.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, role_job_id):
        """Get a role job by id (admin only)"""
        return get_role_job(role_job_id)


@role_job_ns.route("/add")
class AddRolejob(Resource):
    @role_job_ns.header("Authorization" "JWT Token", required=True)
    @role_job_ns.doc(
        summary="Add a Rolejob (admin only)",
        description="This endpoint allows you to add a Rolejob. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, department_id, code, status('0' = Active and '1' = Not Active) and description. The id will be automatically generated. ",
                "schema": {
                    "type": "object",
                    "properties": {
                        "code": {"type": "string"},
                        "name": {"type": "string"},
                        "department_id": {"type": "integer"},
                        "status": {"type": "string"},
                        "description": {"type": "string"},
                    },
                    "required": [
                        "code",
                        "department_id",
                        "name",
                        "status",
                    ],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        """Add a new Rolejob (admin only)"""
        data = request.get_json()
        return add_role_job(data)


@role_job_ns.route("/edit/<int:role_job_id>")
class EditRolejob(Resource):
    @role_job_ns.header("Authorization", "JWT Token", required=True)
    @role_job_ns.doc(
        summary="Update a role job (admin only)",
        description="This endpoint allows you to update a role job. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, department_id, code, status('0' = Active and '1' = Not Active) and description."
                "Only the fields you want to update are required.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "code": {"type": "string"},
                        "department_id": {"type": "integer"},
                        "status": {"type": "string"},
                        "description": {"type": "string"},
                    },
                },
            },
        },
    )
    @jwt_required()
    def put(self, role_job_id):
        """Edit a role job (admin only)"""
        data = request.get_json()
        return update(data, role_job_id)


@role_job_ns.route("/delete/<int:role_job_id>")
class DeleteRolejob(Resource):
    @role_job_ns.header("Authorization", "JWT Token", required=True)
    @role_job_ns.doc(
        summary="Delete an role job",
        description="This endpoint allows you to delete an role job. It is restricted to admins",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, role_job_id):
        """Delete role job by id for admin"""
        return delete(role_job_id)


@role_job_ns.route("/datatable")
class DatatableRolejob(Resource):
    @role_job_ns.header("Authorization", "JWT Token", required=True)
    @role_job_ns.doc(
        summary="Get all role jobs in the database with datatable format (admin only)",
        description="This endpoint returns a list of all role jobs in the database with datatable format. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data Rolejob in Datatable Format"""
        return datatable(request.args)
