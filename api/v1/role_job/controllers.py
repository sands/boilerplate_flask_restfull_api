from flask import jsonify
from .models import *
from ..user.controllers import is_admin, get_user_id_from_token
from app import db
import sys

sys.path.append("..")
from api.v1.common.response import api_response


def get_role_job(role_job_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        role_job = M_role_job.query.filter_by(id=role_job_id).first()
        if role_job:
            role_job = role_job.to_dict()
            return api_response(
                "success", "Rolejob retrieved successfully", role_job, status_code
            )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def get_role_jobs():
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        role_jobs = M_role_job.query.all()
        datas = [e.to_dict() for e in role_jobs]
        return api_response(
            "success", "Rolejobs retrieved successfully", datas, status_code
        )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def add_role_job(data):
    try:
        form = RolejobAddForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            code = data.get("code")
            department_id = data.get("department_id")
            status = data.get("status")
            description = data.get("description")
            rj_check = M_role_job.query.filter_by(code=code).first()

            if rj_check:
                status_code = 400
                return api_response(
                    "error", "Rolejob Taken", response_code=status_code
                )
            last_rj_item = M_role_job.query.order_by(M_role_job.id.desc()).first()
            new_rj_id = last_rj_item.id + 1 if last_rj_item is not None else 1
            post = M_role_job(
                id=new_rj_id,
                name=name,
                department_id=department_id,
                code=code,
                status=status,
                description=description,
                created_by=get_user_id_from_token()
            )
            db.session.add(post)
            db.session.commit()

            return api_response(
                "success", "Add Role Job successfully", post.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def update(data, role_job_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit rolejob", response_code=status_code
            )

        form = RolejobUpdateForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            department_id = data.get("department_id")
            code = data.get("code")
            status = data.get("status")
            description = data.get("description")

            role_job = M_role_job.query.filter_by(id=role_job_id).first()
            role_job.updated_by = get_user_id_from_token()

            if role_job:
                if "name" in data:
                    if M_role_job.query.filter_by(name=name).first():
                        return api_response("error", "Rolejob taken", response_code=400)
                    role_job.name = name
                if "department_id" in data:
                    role_job.department_id = department_id
                if "code" in data:
                    role_job.code = code
                if "description" in data:
                    role_job.description = description
                if "status" in data:
                    role_job.status = status
            else:
                return api_response("error", "Rolejob Not Found", response_code=400)

            db.session.commit()

            return api_response(
                "success", "Update Rolejob successfully", role_job.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def delete(role_job_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete Rolejob", response_code=status_code
            )

        role_job = M_role_job.query.filter_by(id=role_job_id).first()

        if role_job:
            status_code = 200

            # Delete Rolejob
            db.session.delete(role_job)
            db.session.commit()

            return api_response(
                "success", "Delete Rolejob successfully", None, status_code
            )
        else:
            return api_response("error", "Rolejob Not Found", response_code=400)
    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        role_job = M_role_job.query

        if search_value:
            role_job = role_job.filter(M_role_job.name.like(f"%{search_value}%"))

        role_job = role_job.limit(length).offset(start_idx).all()

        # Count the total number of role_jobs
        total_role_job = M_role_job.query.count()

        datas = [e.to_dict() for e in role_job]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_role_job,
            "recordsFiltered": len(role_job),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
