from flask_restx import Namespace, Resource
from .controllers import *
from flask_jwt_extended import jwt_required
from flask import request

position_ns = Namespace("position", description="Master Position User")


@position_ns.route("/all")
class Position(Resource):
    @position_ns.header("Authorization", "JWT Token", required=True)
    @position_ns.doc(
        summary="Get all position in the database (admin only)",
        description="This endpoint returns a list of all position in the database. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get all position in the database (admin only)"""
        return get_positions()


@position_ns.route("/get/<int:position_id>")
class PositionById(Resource):
    @position_ns.header("Authorization" "JWT Token", required=True)
    @position_ns.doc(
        summary="Get position in the database",
        description="This endpoint return position data in the database.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self, position_id):
        """Get a position by id (admin only)"""
        return get_position(position_id)


@position_ns.route("/add")
class AddPosition(Resource):
    @position_ns.header("Authorization" "JWT Token", required=True)
    @position_ns.doc(
        summary="Add a Position (admin only)",
        description="This endpoint allows you to add a Position. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, code, status('0' = Active and '1' = Not Active) and description. The id will be automatically generated. ",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "code": {"type": "string"},
                        "status": {"type": "string"},
                        "description": {"type": "string"},
                    },
                    "required": [
                        "code",
                        "name",
                        "status",
                    ],
                },
            },
        },
    )
    @jwt_required()
    def post(self):
        """Add a new Position (admin only)"""
        data = request.get_json()
        return add_position(data)


@position_ns.route("/edit/<int:position_id>")
class EditPosition(Resource):
    @position_ns.header("Authorization", "JWT Token", required=True)
    @position_ns.doc(
        summary="Update a position (admin only)",
        description="This endpoint allows you to update a position. It is restricted to admins.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            },
            "body": {
                "in": "body",
                "description": "Input name, code, status('0' = Active and '1' = Not Active) and description."
                "Only the fields you want to update are required.",
                "schema": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "code": {"type": "string"},
                        "status": {"type": "string"},
                        "description": {"type": "string"},
                    },
                },
            },
        },
    )
    @jwt_required()
    def put(self, position_id):
        """Edit a position (admin only)"""
        data = request.get_json()
        return update(data, position_id)


@position_ns.route("/delete/<int:position_id>")
class DeletePosition(Resource):
    @position_ns.header("Authorization", "JWT Token", required=True)
    @position_ns.doc(
        summary="Delete an position",
        description="This endpoint allows you to delete an position. It is restricted to admins",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def delete(self, position_id):
        """Delete position by id for admin"""
        return delete(position_id)


@position_ns.route("/datatable")
class DatatablePosition(Resource):
    @position_ns.header("Authorization", "JWT Token", required=True)
    @position_ns.doc(
        summary="Get all positions in the database with datatable format (admin only)",
        description="This endpoint returns a list of all positions in the database with datatable format. It is restricted to admin users only.",
        params={
            "Authorization": {
                "in": "header",
                "description": 'Input your JWT token by adding "Bearer " then your token',
            }
        },
    )
    @jwt_required()
    def get(self):
        """Get Data Position in Datatable Format"""
        return datatable(request.args)
