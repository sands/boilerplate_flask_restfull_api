from app import db
from sqlalchemy import func
from wtforms import Form, StringField
from wtforms.validators import DataRequired, Email
from enum import Enum

PositionStatusDict = {"active": "1", "not_active": "0"}


class M_position(db.Model):
    # Define Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=True)
    status = db.Column(
        db.Enum(PositionStatusDict["not_active"], PositionStatusDict["active"]),
        default=PositionStatusDict["active"],
    )
    created_at = db.Column(db.DateTime, default=func.now(), nullable=False)
    updated_at = db.Column(
        db.DateTime, default=func.now(), onupdate=func.now(), nullable=True
    )
    created_by = db.Column(db.Integer, nullable=False)
    updated_by = db.Column(db.Integer, nullable=True)

    # Relations
    # created_by_user = db.relationship('User', foreign_keys=[created_by], backref=db.backref('created_positions', lazy=True))
    # updated_by_user = db.relationship('User', foreign_keys=[updated_by], backref=db.backref('updated_positions', lazy=True))

    def __repr__(self):
        return f"M_position(code={self.code}, name={self.name}, description={self.description}, status={self.status})"

    def to_dict(self):
        return {
            "id": self.id,
            "code": self.code,
            "name": self.name,
            "description": self.description,
            "status": self.status,
        }


class PositionAddForm(Form):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    status = StringField("Status", validators=[DataRequired()])


class PositionUpdateForm(Form):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    status = StringField("Status", validators=[DataRequired()])
