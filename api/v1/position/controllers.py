from flask import jsonify
from .models import *
from ..user.controllers import is_admin, get_user_id_from_token
from app import db
import sys

sys.path.append("..")
from api.v1.common.response import api_response


def get_position(position_id):
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        position = M_position.query.filter_by(id=position_id).first()
        if position:
            position = position.to_dict()
            return api_response(
                "success", "Position retrieved successfully", position, status_code
            )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def get_positions():
    try:
        status_code = 200
        if not is_admin():
            status_code = 401
            return api_response("error", "Unauthorized", response_code=status_code)

        positions = M_position.query.all()
        datas = [e.to_dict() for e in positions]
        return api_response(
            "success", "Positions retrieved successfully", datas, status_code
        )
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def add_position(data):
    try:
        form = PositionAddForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            code = data.get("code")
            status = data.get("status")
            description = data.get("description")
            post_check = M_position.query.filter_by(code=code).first()

            if post_check:
                status_code = 400
                return api_response(
                    "error", "Position Taken", response_code=status_code
                )
            last_post_item = M_position.query.order_by(M_position.id.desc()).first()
            new_post_id = last_post_item.id + 1 if last_post_item is not None else 1
            post = M_position(
                id=new_post_id,
                name=name,
                code=code,
                status=status,
                description=description,
                created_by=get_user_id_from_token()
            )
            db.session.add(post)
            db.session.commit()

            return api_response(
                "success", "Add position successfully", post.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def update(data, position_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can edit position", response_code=status_code
            )

        form = PositionUpdateForm(data=data)

        if form.validate():
            status_code = 200

            name = data.get("name")
            code = data.get("code")
            status = data.get("status")
            description = data.get("description")

            position = M_position.query.filter_by(id=position_id).first()
            position.updated_by = get_user_id_from_token()

            if position:
                if "name" in data:
                    if M_position.query.filter_by(name=name).first():
                        return api_response("error", "position taken", response_code=400)
                    position.name = name
                if "code" in data:
                    position.code = code
                if "description" in data:
                    position.description = description
                if "status" in data:
                    position.status = status
            else:
                return api_response("error", "Position Not Found", response_code=400)

            db.session.commit()

            return api_response(
                "success", "Update position successfully", position.to_dict(), status_code
            )
        else:
            # Form validation failed
            errors = form.errors
            status_code = 400
            return api_response("error", errors, response_code=status_code)

    except Exception as e:
        db.session.rollback()
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def delete(position_id):
    try:
        if not is_admin():
            status_code = 401
            return api_response(
                "error", "Only admin can delete position", response_code=status_code
            )

        position = M_position.query.filter_by(id=position_id).first()

        if position:
            status_code = 200

            # Delete Position
            db.session.delete(position)
            db.session.commit()

            return api_response(
                "success", "Delete Position successfully", position.to_dict(), status_code
            )
        else:
            db.session.rollback()
            return api_response("error", "Position Not Found", response_code=400)
    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)


def datatable(data):
    try:
        default_draw = 1
        default_page = 1
        default_per_page = 10

        draw = int(data.get("draw", default_draw))
        start = int(data.get("start", default_page))
        length = int(data.get("length", default_per_page))
        search_value = data.get("search").strip() if data.get("search") else ""

        # Calculate pagination offsets
        start_idx = (start - 1) * length

        # Filter the data based on search value
        position = M_position.query

        if search_value:
            position = position.filter(M_position.name.like(f"%{search_value}%"))

        position = position.limit(length).offset(start_idx).all()

        # Count the total number of positions
        total_position = M_position.query.count()

        datas = [e.to_dict() for e in position]

        # Prepare the response in the format required by DataTables
        response = {
            "draw": draw,
            "recordsTotal": total_position,
            "recordsFiltered": len(position),
            "data": datas,
        }
        return jsonify(response)

    except Exception as e:
        status_code = 500
        return api_response("error", str(e), response_code=status_code)
