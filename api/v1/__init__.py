# from flask_cors import CORS
from flask_restx import Namespace, Resource, Api
from flask import Blueprint, abort, jsonify, redirect
from app import app, jwt
from werkzeug.exceptions import HTTPException
from api.v1.user.routes import user_ns
from api.v1.roles.routes import role_ns
from api.v1.modules.routes import module_ns
from api.v1.position.routes import position_ns
from api.v1.role_job.routes import role_job_ns
from api.v1.division.routes import division_ns
from api.v1.unit.routes import unit_ns
from api.v1.department.routes import department_ns
from api.v1.permissions_role.routes import permission_role_ns
from api.v1.menus.routes import menu_ns

# Declare the blueprint
api_v1 = Blueprint("api_v1", __name__)
api = Api(api_v1, version="1.0", title="API v1", description="API v1 endpoints")

# Declare the NameSpace
api.add_namespace(user_ns)
api.add_namespace(role_ns)
api.add_namespace(module_ns)
api.add_namespace(position_ns)
api.add_namespace(role_job_ns)
api.add_namespace(division_ns)
api.add_namespace(unit_ns)
api.add_namespace(department_ns)
api.add_namespace(permission_role_ns)
api.add_namespace(menu_ns)

# Set up cross-scripting allowed
# CORS(api_v1)


# Error Handling
@api.errorhandler(HTTPException)
def handle_error(error: HTTPException):
    """Handle BluePrint JSON Error Response"""
    response = {
        "error": error.__class__.__name__,
        "message": error.description,
    }
    return response, error.code


# Set the default route
@app.route("/swagger")
def swagger_ui():
    return redirect("/static/swagger.html")
